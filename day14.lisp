(load "aoc.lisp")


;; Vectors

(defstruct vec2d
    x
    y
)

(defun % (x y)
    (make-vec2d :x x :y y)
)

(defun %x (vec2d)
    (vec2d-x vec2d)
)

(defun %y (vec2d)
    (vec2d-y vec2d)
)

;; Tiles

(defun %hash (vec2d)
    (+ (* (%x vec2d) 10000) (%y vec2d))
)

(defun get-tile (vec2d)
    (let* (
        (tile (gethash (%hash vec2d) tiles))
    )
    (if (not (null tile))
        tile
        (progn
            (set-tile vec2d :air)
            :air
        )
    )
    )
)



(defun set-tile (vec2d type)
    (let (
        (x (%x vec2d))
        (y (%y vec2d))
        (h (%hash vec2d))
    )
    (progn
        (when (< x minx) (setf minx x))
        (when (> x maxx) (setf maxx x))
        (when (> y maxy) (setf maxy y))
        (setf (gethash h tiles) type)
    )
    )
)

;; Printing
(defun get-tile-char (x y)
    (ecase (get-tile (% x y))
        (:air #\ )
        (:rock #\█)
        (:sand #\0)
    )
)


(defun print-line (y)
    (loop for x from minx to maxx 
        do (format t "~a" (get-tile-char x y))
    )
)

(defun print-block ()
    (loop for y from 0 to maxy 
        do (progn
            (print-line y)
            (terpri)
        )
    )
)

;; Build rocks

(defun build-rocks-rect (x0 y0 x1 y1)
    (loop for x from x0 to x1 do
        (loop for y from y0 to y1 do
            (set-tile (% x y) :rock)
        )
    )
)

(defun build-rocks-cleaned (x0 y0 x1 y1)
    (let (
        (min-x (min x0 x1))
        (max-x (max x0 x1))
        (min-y (min y0 y1))
        (max-y (max y0 y1))
    )
        (build-rocks-rect min-x min-y max-x max-y)
    )
)

(defun build-snake (snake)
    (if (< (length snake) 4 )
        nil
        (progn
        (let (
            (x0 (car snake))
            (y0 (cadr snake))
            (x1 (caddr snake))
            (y1 (cadddr snake))
        )
            (build-rocks-cleaned x0 y0 x1 y1)
        )
        (build-snake (cddr snake))
        )
    )
)

(defun build-snake-from-string (string)
    (build-snake (extract-numbers string))
)

(defun reset-field ()
    (progn
        (setq tiles (make-hash-table))
        (setq minx 499)
        (setq maxx 501)
        (setq maxy 0) 
        (loop for line in (load-input 14) do (build-snake-from-string line))
        (setf maxyrock maxy)
    )
)



(pdebug "got here")
;; Falling

(defun tile-is-air (pos)
    (eql (get-tile pos) :air)
)


(defun below (xy)
    (% (%x xy) (+ (%y xy) 1))
)

(defun below-left (xy)
    (% (- (%x xy) 1) (+ (%y xy) 1))
)

(defun below-right (xy)
    (% (+ (%x xy) 1) (+ (%y xy) 1))
)

(defun air-tile (tiles)
    (find-if #'tile-is-air tiles)
)

(defun next-pos (xy)
    (air-tile (list (below xy) (below-left xy) (below-right xy)))
)



(defun final-pos (xy floor)
    (if (>= (%y xy) (+ maxyrock 1))
        (if floor xy nil)
        (let (
            (next (next-pos xy))
        )
        (if (null next)
            xy
            (final-pos next floor)
        )
        )
    )
)

(defun drop-sand-if-possible (floor)
    (let (
        (pos (final-pos (% 500 0) floor))
    )
    (if (null pos)
        nil
        (if (tile-is-air pos)
            (progn
                (set-tile pos :sand)
                ;; (format t "dropped on (~a, ~a)" (%x pos) (%y pos))
                ;; (terpri)
                t
            )
            nil
        )
    )
    )
)

(defun drop-sand-while-possible (floor)
    
    (setf counter 0)
    (loop while t do
        (let (
            (dropped (drop-sand-if-possible floor))
        )
        (if (null dropped)
            (return-from drop-sand-while-possible counter)
            (setf counter (+ counter 1))
        )
        )
    )
)

(reset-field)
(setf p1 (drop-sand-while-possible nil))
(print-block)
(setf p2 (drop-sand-while-possible t))
(print-block)
(sol p1 (+ p1 p2))