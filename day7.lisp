(load "aoc.lisp")

(defun tokenize (line)
    "Splits a command string into tokens."
    (cl-ppcre:split " " line)
)

(defun compress-command (command)
    "rewrites a command such that the full command is the first argument
    for example, it will rewrite $ cd / to $cd /
    "
    (if (string= (car command) "$") (cons (concatenate 'string (car command) (cadr command)) (cddr command)) command)
)

(defun tokenize-and-compress (line)
    (compress-command (tokenize line))
)


(defun command-cd (cwd arg0)
    "Calculates the new cwd given the old cwd and the first argument of a cd command"
    (if (string= arg0 "/")
        nil
        (if (string= arg0 "..")
            (cdr cwd) (cons arg0 cwd)
        )
    )
)

(defun preproq-single-command (cwd command)
    "Preprocesses a single command excluding cd given the current working directory
        1. It replaces ls commands with (:dir cwd)
        2. It replaces 'dir <name>' outputs with (:subdir <fullpath>)
        3. It replaces file outputs with (:file <name> <size>)
    "
    (if (and (string= (car command) "$ls"))
        (list :setcwd cwd)
        (if (string= (car command) "dir")
            (list :mkdir (cons (cadr command) cwd))
            (list :mkfile (cadr command) (car command))
        )
    )
)

(defun preproq-helper (cwd commands)
    "Preprocesses a list of tokenized commands. 
        1. If removes cd commands (the information is tranfered to ls)
        2. Applies the transformations defined in preproq-single-command
    It does this recusively cause F*** for loops or somethings
    "
    (if (null commands) nil
        (if (and (string= (caar commands) "$cd"))
            (preproq-helper (command-cd cwd (cadar commands)) (cdr commands))
            (cons (preproq-single-command cwd (car commands)) (preproq-helper cwd (cdr commands)))
        )
    )
)

(defun preproq-input ()
    "tokenizes and preprocesses the input file"
    (preproq-helper nil (mapcar #'tokenize-and-compress (load-input 7)))
)

(defun str-path (path)
    "convert a path stored as a stack to a path stored as a string"
    (format nil "/~{~A~^/~}" (reverse path))
)

(defstruct &file
    "file object"
    filename
    size
)

(defstruct &dir
    "dir object"
    path
    subdirs
    files
)

(defun with-new-file (dirobject fileobject)
    "returns a directory object similar to the original one but with an extra file"
    (make-&dir 
        :path (&dir-path dirobject)
        :subdirs (&dir-subdirs dirobject)
        :files (cons fileobject (&dir-files dirobject))
    )
)

(defun with-new-subdir (dirobject subdirpath)
    "returns a directory object similar to the original one but with an extra subdirectory"
    (make-&dir 
        :path (&dir-path dirobject)
        :subdirs (cons subdirpath (&dir-subdirs dirobject))
        :files (&dir-files dirobject)
    )
)

(defun with-new-file-in-current-dir (table fileobject)
    "returns a filesystem table with a new file in the current directory"
    (cons (with-new-file (car table) fileobject) (cdr table))
)

(defun with-new-dir-in-current-dir (table path)
    "returns a filesystem table with a new subdir in the current directory"
    (cons (with-new-subdir (car table) (str-path path)) (cdr table))
)

(defun with-new-dir (table path)
    "returns a filesystem table with a different current directory"
    (cons (make-&dir :path (str-path path) :subdirs nil :files nil) table)
)

(defun make-file (command)
    "Build a file object from a (:file ...) command"
    (make-&file
        :filename (cadr command)
        :size (parse-integer (caddr command))
    )
)

(defun build-table (commands)
    "Build a filesystem table from all preprocessed commands"
    (if (null commands) nil
        (ecase (caar commands)
            (:setcwd (with-new-dir (build-table (cdr commands)) (cadar commands)))
            (:mkdir (with-new-dir-in-current-dir (build-table (cdr commands)) (cadar commands)))
            (:mkfile (with-new-file-in-current-dir (build-table (cdr commands)) (make-file (car commands))))
        )
    )
)

"store the filesystem table in a golbal command so it only has to be calculated once"
(setq fstable (build-table (reverse (preproq-input))))


(defun retrieve (path)
    "Returns a directory object given a path"
    (find-if #'(lambda (x) (string= path (&dir-path x))) fstable)
)

(defun dir-direct-size (path)
    "Returns the total size of all files in a directory, exclusing subdirectories"
    (reduce #'+ (mapcar #'&file-size (&dir-files (retrieve path))))
)

(defun dir-total-size (path)
    "Returns the total size of all files in a directory, including subdirectories"
    (+ (reduce #'+ (mapcar #'dir-total-size(&dir-subdirs (retrieve path)))) (dir-direct-size path))
)

(defun all-paths ()
    "Returns all exiting paths for directories"
    (mapcar #'&dir-path fstable)
)

(defun part1-correct-dirsize (size)
    "Sets the size of files that are larger than 100000 to 0"
    (if (> size 100000) 0 size)
)

(defun part1 ()
    "Solves part 1"
    (reduce #'+ (mapcar #'part1-correct-dirsize (mapcar #'dir-total-size (all-paths))))
)

(setq used-space (dir-total-size "/"))

(defun free-space-after-deletion-minus-update (path)
    "returns the diskspace that would be left after the directory is deleted, exclusing the space needed by the update. 
    Negative numbers "
    (- 40000000 (- used-space (dir-total-size path)))
)

(defun deleting-helps (path)
    "returns if deleting the folder frees enough space for the update"
    (> (free-space-after-deletion-minus-update path) 0)
)

(defun filter-delete ()
    "returns only the paths that can be deleted"
    (loop for path in (all-paths)
        when (deleting-helps path) 
        collect path
    )
)

(defun smallest-folder-pair (d1 d2)
    "returns the smallest directory given two of them"
    (if (< (dir-total-size d1) (dir-total-size d2)) d1 d2)
)

(defun best-folder-to-delete ()
    "returns the smallest folder that can be deleted"
    (reduce #'smallest-folder-pair (filter-delete))
)

(defun part2 ()
    "Solves part 2"
    (dir-total-size (best-folder-to-delete))
)


(pdebug (mapcar #'tokenize (load-input 7)))
(pdebug (mapcar #'tokenize-and-compress (load-input 7)))
(pdebug (preproq-input))
(pdebug fstable)

(sol (part1) (part2))

