(load "aoc.lisp")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PREPOCESSING INPUT
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun clean-input ()
    "splits the input of day 9 into lists of the direction and the distance"
    (mapcar #'extract-words (load-input 9))
)

(defun extract-direction (inputkey)
    "converts the input string into a movement atom (:l :r :u :d)"
    (if (string= inputkey "R") :r (if (string= inputkey "L") :l (if (string= inputkey "U") :u (if (string= inputkey "D") :d :n))))
)

(defun preproq-input-line (line)
    "breaks down the input in to single movement atoms"
    (make-list (parse-integer (cadr line)) :initial-element (extract-direction (car line)))
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 2d vectors
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defstruct vec2d
    x
    y
)

(defun v2d (x y)
    "shorter version to build a vec2d"
    (make-vec2d :x x :y y)
)

(defun movement-to-vec2d (movement)
    "convert a movement atom to a difference vector"
    (ecase movement
        (:r (v2d 1 0))
        (:l (v2d (- 1) 0))
        (:u (v2d 0 1))
        (:d (v2d 0 (- 1)))
    )
)

(defun vector- (vec)
    "refelcts a vector over the origin"
    (make-vec2d :x (- (vec2d-x vec)) :y (- (vec2d-y vec)))
)

(defun vector+ (vec1 vec2)
    "sums two vectors"
    (make-vec2d :x (+ (vec2d-x vec1) (vec2d-x vec2)) :y (+ (vec2d-y vec1) (vec2d-y vec2)))
)

(defun vector= (vec1 vec2)
    "checks if two vectors are equal"
    (and (= (vec2d-x vec1) (vec2d-x vec2)) (= (vec2d-y vec1) (vec2d-y vec2)))
)

(defun input-as-vectors ()
    "loads the input as a list of input vectors"
    (mapcar #'movement-to-vec2d (mapcan #'preproq-input-line (clean-input)))
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; movements and paths
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun cumulative-reduce (func seq init)
    "reduce but returns a list of temp results"
    (if (null seq) (list init)
        (cons 
            init 
            (cumulative-reduce func (cdr seq) (apply func (list (car seq) init)))
        )
    )
)

(defun travel (currpos v2ds)
    "supplied with a list of input vectors and a starting position, returns the travel path"
    (cumulative-reduce #'vector+ v2ds currpos)
)

(defun travel0 (v2ds)
    "supplied with a list of input vectors, returns the travel path if the starting position is (v2d 0 0)"
    (travel (v2d 0 0) v2ds)
)

(defun extract-movement (locations)
    "returns the relative movement vectors given the absolute positions"
    (if (null (cdr locations)) nil
        (cons 
            (vector+ (vector- (car locations)) (cadr locations))
            (extract-movement (cdr locations))
        )
    )
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; relative position of tail
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun chebyshev (vec)
    "returns the chebyshev metric of a vector"
    (max (abs (vec2d-x vec)) (abs (vec2d-y vec)))
)

(defun press-to-center-1dim (val)
    "moves a integeter one step towards zero"
    (if (< val 0) (+ val 1) (if (> val 0) (- val 1) val))
)

(defun mapvec (func loc)
    "applies a function to each coordinate of a vector"
    (v2d (apply func (list (vec2d-x loc))) (apply func (list (vec2d-y loc))))
)

(defun press-to-center (loc)
    "moves a vecotr one diagonal step towards zero (or stright oif one of the dims is already 0"
    (mapvec #'press-to-center-1dim loc)
)

(defun reposition-tail (loc)
    "reposition the tail given its relative position to the head as described in the puzzle"
    (if (<= (chebyshev loc) 1) loc (press-to-center loc))
)


(defun next-position-tail (movehead currpos)
    "returns the next position of the tail given the current position and the movement of the head"
    (reposition-tail (vector+ currpos (vector- movehead)))
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; absolute position of tail
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun travel-tail (currrelpos v2ds)
    "given the travel instructions of the head, return a list of the relative positions of the tail"
    (cumulative-reduce #'next-position-tail v2ds currrelpos)
)

(defun travel0-tail (v2ds)
    "same as travel tail but starts at position (0 0)"
    (travel-tail (v2d 0 0) v2ds)
)

(defun follow (headpositions)
    "given the absolute positions of the head, return the respective absoulte positions of the tail"
    (mapcar #'vector+ (travel0-tail (extract-movement headpositions)) headpositions)
)

(defun follow-snake (headpositions depth)
    "given the absolute positions of the head, return the respective absoulte positions of the nth segment of the snake"
    (if (= depth 0) headpositions (follow-snake (follow headpositions) (- depth 1)))
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; count number of unique positions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun hash-int-to-nat (int)
    "convert any integer to a unique natural number"
    (if (> int 0) (* 2 int) (+ (* (- int) 2) 1))
)

(defun hash-vec-to-int (loc)
    "convert any vector to a unique natural number"
    (+ (hash-int-to-nat (vec2d-x loc)) (* 1000 (hash-int-to-nat (vec2d-y loc))))
)

(defun count-unique-fields (seq)
    "count the number of unique vectors"
    (length (remove-duplicates (mapcar #'hash-vec-to-int seq) :test #'=))
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; main part and debug statements
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq input-as-vectors (input-as-vectors))

(defun track-snake-segment (depth)
    "returns the absolute position over time of the nth segment in the snake using the movement from the file."
    (count-unique-fields (follow-snake (travel0 input-as-vectors) depth))
)

(sol (track-snake-segment 1) (track-snake-segment 9))

