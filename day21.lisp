(load "aoc.lisp")

(defun load-pair (line)
    (apply #'cons (cl-ppcre:split ": " line))
)

(setf p2 t)

(defun extract-commands (line)
    (let (
        (name (car line))
        (expr (cl-ppcre:split " " (cdr line)))
    )
    (if (and (string= name "root") p2)
        (cons name (list '= (car expr) (caddr expr)))
        (if (null (cdr expr))
            nil
            (cons name (list (read-from-string (cadr expr)) (car expr) (caddr expr)))
        )
    )
    )
)

(defun expand-commands (line)
    (let (
        (a0 (car line))
        (sym (cadr line))
        (a1 (caddr line))
        (a2 (cadddr line))
    )
    (case sym
        ('+ (list line (list a1 '- a0 a2) (list a2 '- a0 a1)))
        ('* (list line (list a1 '/ a0 a2) (list a2 '/ a0 a1)))
        ('- (list line (list a1 '+ a0 a2) (list a2 '- a1 a0)))
        ('/ (list line (list a1 '* a0 a2) (list a2 '/ a1 a0)))
        ('= (list (list a1 '+ a2 "zero--") (list a2 '+ a1 "zero--")))
    )
    )
)

(defun extract-values (line)
    (let (
        (name (car line))
        (expr (cl-ppcre:split " " (cdr line)))
    )
    (if (and (null (cdr expr)) (or (not p2) (not (string= name "humn"))))
        (cons name (parse-integer (car expr)))
        nil
    )
    )
)

(setq commands (mapcan #'expand-commands (remove nil (mapcar #'extract-commands (mapcar #'load-pair (load-input 21))))))

(setq knowvars (acons "zero--" 0 (remove nil (mapcar #'extract-values (mapcar #'load-pair (load-input 21))))))

(defun expand (command)
    (let* (
        (a0 (car command))
        (sym (cadr command))
        (a1 (caddr command))
        (a2 (cadddr command))
        (v1 (assoc a1 knowvars :test #'string=))
        (v2 (assoc a2 knowvars :test #'string=))
    )
    (when (null (assoc a0 knowvars :test #'string=))
    (when (and (not (null v1)) (not (null v2)))
        (progn
            (setf knowvars 
                (acons 
                    a0
                    (funcall sym (cdr v1) (cdr v2))
                    knowvars
                )
            )
            (format t "~a" (assoc a0 knowvars))
            (terpri)
        )
    )
    )
    )
)

(loop while (null (assoc (if p2 "humn" "root") knowvars :test #'string=)) do
    (mapcar #'expand commands)
)