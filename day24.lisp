(load "dstar.lisp")




(defstruct blizzard 
    loc
    dir
)

(defparameter char-to-dir (list
    (cons #\< :left)
    (cons #\> :right)
    (cons #\^ :up)
    (cons #\v :down)
))

(defun init-blizzard ()
    (mapcan #'identity (loop for x in (build-2d-map (load-input 24)) collect
        (let* (
            (loc (list (- (caar x) 1) (- (cadar x) 1)))
            (char (cdr x))
            (atom (cdr (assoc char char-to-dir)))
        )
        (if (not (null atom))
            (list (make-blizzard :loc loc :dir atom))
            nil
        )
        )
    )
    )
)

(defparameter blizzard0 (init-blizzard))


(defparameter minx (reduce #'min (mapcar #'car  (mapcar #'blizzard-loc blizzard0))))
(defparameter maxx (reduce #'max (mapcar #'car  (mapcar #'blizzard-loc blizzard0))))
(defparameter miny (reduce #'min (mapcar #'cadr (mapcar #'blizzard-loc blizzard0))))
(defparameter maxy (reduce #'max (mapcar #'cadr (mapcar #'blizzard-loc blizzard0))))

(defparameter sizex (+ maxx 1))
(defparameter sizey (+ maxy 1))
(defparameter size-t (lcm sizex sizey))

(defun move-blizzard (current)
    (destructuring-bind (x y) (blizzard-loc current)
    (destructuring-bind (dx dy)
    (ecase (blizzard-dir current)
        (:up (list 0 (- 1)))
        (:down (list 0 1))
        (:left (list (- 1) 0))
        (:right (list 1 0))
    )
    
    (make-blizzard 
        :loc (list (mod (+ x dx) sizex) (mod (+ y dy) sizey))
        :dir (blizzard-dir current)
    )
    ))
)

(defun char-blizzard-at (pos all)
    (if (> (count pos (mapcar #'blizzard-loc all) :test #'equal) 1)
        (count pos (mapcar #'blizzard-loc all) :test #'equal)
    (if (= (count pos (mapcar #'blizzard-loc all) :test #'equal) 1)
        (car (rassoc 
            (blizzard-dir (find-if (lambda (x) (equal (blizzard-loc x) pos)) all)) char-to-dir))
        #\.
    ))
)

(defun print-state (allpos)
    (loop for y from miny to maxy do
        (loop for x from minx to maxx do
            (format t "~a" (char-blizzard-at (list x y) allpos))
        )
        (terpri)
    )
    (terpri)
)

(defparameter blizzard-table (make-hash-table))
(defparameter blizzard-store (make-hash-table))

(defun hash-state (time- x y)
    (+ 
        (* 1000000 time-)
        (* 1000 x)
        y
    )
)

(defun compile-blizzard-to-hashtable (index blizzards)
    (loop for bliz in blizzards do
        (let (
            (x (first (blizzard-loc bliz)))
            (y (second (blizzard-loc bliz)))
        )
            (setf (gethash (hash-state index x y) blizzard-table) t)
        )
    )
)

(defparameter blizzards (list blizzard0))
(loop for index from 0 to (- size-t 1) do
    (setf blizzard0 (mapcar #'move-blizzard blizzard0))
    (setf (gethash index blizzard-store) blizzard0)
    ;;(pdebug blizzards)
    ;;(print-state blizzard0)
    (format t "preproq blizzard: ~a~C" index #\Return)
    (compile-blizzard-to-hashtable index blizzard0)
)

(defun get-blizzard-by-ts (ts)
    (gethash (mod ts size-t) blizzard-store)
)


(defstruct party
    time-
    x
    y
)

(defun hashparty (party-)
    (hash-state 
            (party-time- party-)
            (party-x party-)
            (party-y party-)
        )
)

(defun is-state-not-in-blizzard (party-)
    (not (gethash 
        (hash-state 
            (mod (party-time- party-) size-t)
            (party-x party-)
            (party-y party-)
        )
        blizzard-table
    ))
)

(defun is-state-valid (party-)
    (and
        (is-state-not-in-blizzard party-)
        (>= (party-x party-) 0)
        (>= (party-y party-) 0)
        (< (party-x party-) sizex)
        (< (party-y party-) sizey)
    )
)

(defun filter (predicate seq)
    (mapcan #'(
        lambda (element)
        (if (apply predicate (list element))
            (list element)
            nil
        )
    )
    seq
    )
)


(defun next-states (current)
    (filter #'is-state-valid (list
        (make-party 
            :time- (+ (party-time- current) 1) 
            :x (+ (party-x current) 1)
            :y (party-y current)
        )
        (make-party 
            :time- (+ (party-time- current) 1) 
            :x (- (party-x current) 1)
            :y (party-y current)
        )
        (make-party 
            :time- (+ (party-time- current) 1) 
            :x (party-x current)
            :y (+ (party-y current) 1)
        )
        (make-party 
            :time- (+ (party-time- current) 1) 
            :x (party-x current)
            :y (- (party-y current) 1)
        )
        (make-party 
            :time- (+ (party-time- current) 1) 
            :x (party-x current)
            :y (party-y current)
        )
    ))
)

(defun wait-ts (current)
    (make-party 
            :time- (+ (party-time- current) 1) 
            :x (party-x current)
            :y (party-y current)
    )
)

(defun print-state-full (party)
    (let (
        (px (party-x party))
        (py (party-y party))
        (allpos (get-blizzard-by-ts (party-time- party)))
    )    
    (progn
        (format t "~a" (party-time- party))
        (terpri)
    (loop for y from miny to maxy do
        (loop for x from minx to maxx do
            (if (and (= px x) (= py y))
                (format t "~a" "E")
                (format t "~a" (char-blizzard-at (list x y) allpos))
            )
        )
        (terpri)
    )
    (terpri)
    )
    )
)

(defun branch (states)
    (remove-duplicates (mapcan #'next-states states) :test #'equal)
)



(defun heuristic (state)
    ;;(format t "heuristic: ~a" state)
    (+ 
        (* 10000 (+
            (party-time- state)
            (- maxx (party-x state))
            (- maxy (party-y state))
        ))
        (party-time- state)
    )
)

(defun solution (state)
    ;;(format t "solution: ~a" state)
    (and
        (= (party-x state) maxx)
        (= (party-y state) maxy)
    )
)

(defun heuristic2 (state)
    ;;(format t "heuristic: ~a" state)
    (+ 
        (* 10000 (+
            (party-time- state)
            (party-x state)
            (party-y state)
        ))
        (party-time- state)
    )
)

(defun solution2 (state)
    ;;(format t "solution: ~a" state)
    (and
        (= (party-x state) 0)
        (= (party-y state) 0)
    )
)

(defun solvedstar (state-)
    (wait-ts (wait-ts (d-star 
        (start-state-group state- 20)
        #'hashparty
        #'heuristic
        #'solution
        #'next-states
    )))
)

(defun solvedstar2 (state-)
    (wait-ts (wait-ts (d-star 
        (start-state-group state- 20)
        #'hashparty
        #'heuristic2
        #'solution2
        #'next-states
    )))
)


(defun solve (states)
    (defparameter curstates states)
    (loop while t do
        (setf curstates (branch curstates))
        ;;(print-state-full (car curstates))
        (format t "~a ~a" (party-time- (car curstates)) (length curstates))
        (terpri)
        (let (
            (final (find-if #'(lambda (x)
            (and 
                (= (party-x x) maxx)
                (= (party-y x) maxy)
            )
        ) 
        curstates 
        )
        ))
        (when (not (null final))
            (return-from solve final)
        )
        )
    )
)

(defun start-state (index)
    (let (
        (party (make-party :time- index :x 0 :y 0))
    )
    (if (is-state-valid party)
        party
        (start-state (+ index 1))
    )
    )
)

(defun start-state-from-party (party)
    (if (is-state-valid party)
        party
        (start-state-from-party (wait-ts party))
    )
)

(defun start-state-group (party len)
    (pdebug party)
    (let ((nxt (start-state-from-party party)))
    (if (> len 0) (cons nxt (start-state-group (wait-ts nxt) (- len 1))) nil)
    )
)

(pdebug (solvedstar (solvedstar2 (solvedstar (start-state 1)))))




