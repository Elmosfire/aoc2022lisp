(load "aoc.lisp")

(defun alias% (as symbol &key (package *package*))
  (when (fboundp symbol)
    (setf (symbol-function as) (symbol-function symbol)))
  (when (boundp symbol)
    (setf (symbol-value as) (symbol-value symbol)))
  (setf (symbol-plist as) (symbol-plist symbol))
  ;; maybe also documentation of all types
  (shadowing-import as package))

(defmacro defalias (as symbol &key (package *package*))
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (alias% ',as ',symbol :package ,package)))

(defun λ-reader (stream char)
  (declare (ignore char stream))
  'LAMBDA)

(set-macro-character #\λ #'λ-reader)

(defun φ-reader (stream char)
  (declare (ignore char stream))
  'defun)

(set-macro-character #\φ #'φ-reader)

(defun Ξ-reader (stream char)
  (declare (ignore char stream))
  'setf)

(set-macro-character #\Ξ #'Ξ-reader)


(defmacro γ (args &body body) `(apply ,args ,@body))
(defmacro ψ (args &body body) `(mapcar ,args ,@body))
(defmacro ψ+ (args &body body) `(mapcan ,args ,@body))
(defmacro &λ (args &body body) `(list ,args ,@body))
(defalias xn extract-numbers)
(defalias xi load-input)
(defalias -# count)
(defalias ++++ cons)
(defalias >>>> sol)
(defalias Σ1 length)
(defalias // floor)
(defalias % rem)
(defalias τ= make-hash-table)
(defalias τ+ gethash)
(defalias =∅ null)

(setf ∅ nil)
