(load "aoc.lisp")



(defun convert-line (line)
    "convert a line ex 'A X' to a pair of ints ex (0 2)"
    (list (char-index #\A (char line 0)) (char-index #\X (char line 2)))
)

(defun calc-win (game) 
    "returns 0 if lost, 1 if draw, and 2 if win"
    (rem (+ (- (nth 1 game) (car game)) 4) 3)
)

(defun calc-player-move (game)
    "given part 2, calculate the actual moved played from (opponent move, intended outcome)"
    (rem (+ (nth 1 game) (car game) 2) 3)
)

(defun fix-game (game)
    "convert part 2 style game to part 1 style games"
    (list (car game) (calc-player-move game))
)
   
(defun calc-score-total (game) 
    "convert game into scores"
    (+ (* (calc-win game) 3) (nth 1 game) 1)
)

(format t "part1: ~a" (reduce #'+ (mapcar #'calc-score-total (mapcar #'convert-line (load-input 2)))))
(terpri)
(format t "part2: ~a" (reduce #'+ (mapcar #'calc-score-total (mapcar #'fix-game (mapcar #'convert-line (load-input 2))))))
(terpri)





