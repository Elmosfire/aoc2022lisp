(load "aoc.lisp")

(ql:quickload :split-sequence)

(defun tokenize (line)
    "Splits a command string into tokens."
    (cl-ppcre:split " " line)
)

(defun to_add_instr (line)
    (if (string= (car line) "noop") (list 0) (list 0 (parse-integer (cadr line))))
)

(setq state (cumulative-reduce #'+ (mapcan #'to_add_instr (mapcar #'tokenize (load-input 10))) 1))

(defun signal-strength (index)
    (* index (nth (- index 1) state))
)

(defun enumerate-with-pointer (seq)
    (loop for index from 0 to (- (length seq) 1)
        do collect (list (rem index 40) (nth index seq))
    )
)

(defun is-visible (struct)
    (< (abs (- (car struct) (cadr struct))) 2)
)

(defun pixel (struct)
    (if (is-visible struct) #\█ #\  )
)

(defun line (start)
    (apply #'concatenate (cons 'string (mapcar 'string (mapcar #'pixel (subseq (enumerate-with-pointer state) start (+ start 40))))))
)



(setq p1 (reduce #'+ (mapcar #'signal-strength (list 20 60 100 140 180 220))))


(pdebug (enumerate-with-pointer state))

(sol p1 "see below")

(format t "~a" (line 0)) (terpri)
(format t "~a" (line 40)) (terpri)
(format t "~a" (line 80)) (terpri)
(format t "~a" (line 120)) (terpri)
(format t "~a" (line 160)) (terpri)
(format t "~a" (line 200)) (terpri)
