(load "aoc.lisp")


(defun sanitise-height-char (char)
    (if (char= #\S char) #\a (if (char= #\E char) #\z char))
)

(defun char-to-height (char)
    (char-index #\a (sanitise-height-char char))
)

(defun char-to-property (char)
    (if (char= #\S char) :start (if (char= #\E char) :end nil))
)


(defun push-prefix (item seq)
    (loop for element in seq 
        collect (cons item element)
    )
)


(defun sys-map-input ()
    (let* (
        (inp (load-input 12))
    )
        (mapcan 
            #'(lambda (x) (apply #'push-prefix x)) 
            (enumerate 
                (mapcar #'enumerate (mapcar #'to-char-list inp))
            )
        )
    )
)

(defstruct tile
    x
    y
    height
    property
    flood
)

(defun build-tile (x y c)
    (make-tile :x x :y y :height (char-to-height c) :property (char-to-property c))
)

(setq tiles (mapcar #'(lambda (x) (apply #'build-tile x)) (sys-map-input)))

(defun find-tile (x y)
    (find-if #'(lambda (tile) (and (= (tile-x tile) x) (= (tile-y tile) y))) tiles)
)

(defun find-start-tile ()
    (find-if #'(lambda (tile) (eql (tile-property tile) :start)) tiles)
)

(defun find-end-tile ()
    (find-if #'(lambda (tile) (eql (tile-property tile) :end)) tiles)
)

(defun flood-tile (tile val)
    (setf (tile-flood tile) val)
)

(defun get-flooded-tiles (val)
    (mapcan 
        #'(lambda (tile) 
        (if (and (not (null (tile-flood tile))) (= (tile-flood tile) val))
            (list tile)
            nil
        )
        ) 
    tiles
    )
)

(defun neightbor-tiles (tile)
    (let (
        (x (tile-x tile))
        (y (tile-y tile))
    )
    (list
        (find-tile x (+ y 1))
        (find-tile x (- y 1))
        (find-tile (+ x 1) y)
        (find-tile (- x 1) y)
    )
    )
)

(defun accessible (tile1 tile2)
    (<= (- (tile-height tile2) (tile-height tile1)) 1)
)

(defun acc-neightbor-tiles (tile)
    (mapcan 
        #'(lambda (newtile) 
        (if (and (not (null newtile)) (accessible tile newtile) (null (tile-flood newtile)))
            (list newtile)
            nil
        )
        ) 
    (neightbor-tiles tile)
    )    
)

(setf glob-fill-level 0)
(defun expand ()
    (let* (
        (old-flood-level (- glob-fill-level 1))
        (new-tiles (mapcan #'acc-neightbor-tiles (get-flooded-tiles old-flood-level)))
    )
    (progn
        (loop for tile in new-tiles do
            (flood-tile tile glob-fill-level)
        )
        (setf glob-fill-level (+ glob-fill-level 1))
        (pdebug glob-fill-level)
    )
    )
)

(defun end-flood-level ()
    (tile-flood (find-end-tile))
)

(defun find-bottom-tiles ()
    (mapcan 
        #'(lambda (tile) 
        (if (= (tile-height tile) 0)
            (list tile)
            nil
        )
        ) 
    tiles
    )
)



;;(flood-tile (find-start-tile) 0)
(mapcar #'(lambda (x) (flood-tile x 0)) (find-bottom-tiles))
(loop while (null (end-flood-level)) do
    (expand)
)

(pdebug (end-flood-level))