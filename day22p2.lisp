(load "aoc.lisp")

(defun parse-map-tile (tile)
    (cdr (assoc tile (list (cons #\  :warp) (cons #\. :open) (cons #\# :wall)) :test #'char=))
)

(defparameter tilesize 50)
(defparameter tiles (expand-enumerate-2d (list (list nil :A :B) (list nil :C nil) (list :D :E nil) (list :F nil nil))))

(pdebug tiles)

(defun to-tile-location (pos)
    (let* (
        (x (car pos))
        (y (cadr pos))
        (rx (floor x tilesize))
        (ry (floor y tilesize))
        (dx (mod x tilesize))
        (dy (mod y tilesize))
    )
        (list (cdr (assoc (list rx ry) tiles :test #'equal)) dx dy)
    )
)


(defparameter fieldmap (build-2d-map (reverse (cdr (reverse (load-input 22))))))
(defparameter field (mapcar #'(lambda (x) (cons (to-tile-location (car x)) (parse-map-tile (cdr x)))) fieldmap))

(defparameter xloop (+ (reduce #'max (mapcar #'caar fieldmap)) 1))
(defparameter yloop (+ (reduce #'max (mapcar #'cadar fieldmap)) 1))

(pdebug field)
(defun parse-movement-instruction (instruction)
    (if (string= instruction "L")
        (list 0 0)
        (if (string= instruction "R")
            nil
            (list (parse-integer instruction))
        )
    )
)



(defparameter instructions (mapcan #'parse-movement-instruction (cl-ppcre:all-matches-as-strings "([LR]|[0-9]+)" (car (reverse (load-input 22))))))

(pdebug instructions)

(defun neighbor-ignore-bounds (pos direction)
    (let (
        (tile (car pos))
        (x (cadr pos))
        (y (caddr pos))
    )
    (ecase direction
        (:left (list tile (- x 1) y))
        (:right (list tile (+ x 1) y))
        (:up (list tile x (- y 1)))
        (:down (list tile x (+ y 1)))
    )
    )
)

(defun out-of-bounds (pos direction)
    (let (
        (tile (car pos))
        (x (cadr pos))
        (y (caddr pos))
    )
    (if (or (< x 0) (>= x tilesize))
        (list tile y)
    (if (or (< y 0) (>= y tilesize))
        (list tile x)
    nil
    )))
)

(defun force-into-bounds-helper (targettile direction dist)
    (cons targettile 
    (ecase direction
        (:left (list (- tilesize 1) dist))
        (:right (list 0 dist))
        (:up (list dist (- tilesize 1)))
        (:down (list dist 0))
    )
    )
)

(defun force-into-bounds (targettile direction dist rev)
    (if rev 
        (force-into-bounds-helper targettile direction (- (- tilesize 1) dist))
        (force-into-bounds-helper targettile direction dist)
    )
)

(defun translate-out-of-bounds (tile direction)
    (ecase tile
        (:A 
            (ecase direction
                (:left (list :D :right t))
                (:right (list :B :right nil))
                (:up (list :F :right nil))
                (:down (list :C :down nil))
            )
        )
        (:B 
            (ecase direction
                (:left (list :A :left nil))
                (:right (list :E :left t))
                (:up (list :F :up nil))
                (:down (list :C :left nil))
            )
        )
        (:C 
            (ecase direction
                (:left (list :D :down nil))
                (:right (list :B :up nil))
                (:up (list :A :up nil))
                (:down (list :E :down nil))
            )
        )
        (:D
            (ecase direction
                (:left (list :A :right t))
                (:right (list :E :right nil))
                (:up (list :C :right nil))
                (:down (list :F :down nil))
            )
        )
        (:E
            (ecase direction
                (:left (list :D :left nil))
                (:right (list :B :left t))
                (:up (list :C :up nil))
                (:down (list :F :left nil))
            )
        )
        (:F
            (ecase direction
                (:left (list :A :down nil))
                (:right (list :E :up nil))
                (:up (list :D :up nil))
                (:down (list :B :down nil))
            )
        )
    )
)

(defun get-real-tile (pos direction)
    (let (
        (oob (out-of-bounds pos direction))
    )
    (if (null oob)
        (list pos direction)
        (let* (
            (origtile (car oob))
            (dist (cadr oob))
            (target (translate-out-of-bounds origtile direction ))
            (targettile (car target))
            (targetdir (cadr target))
            (rev (caddr target))
            (targetpos (force-into-bounds targettile targetdir dist rev))
        )
        (list targetpos targetdir)
        )
    )
    )
)

(defun get-target-tile (pos direction)
    (get-real-tile (neighbor-ignore-bounds pos direction) direction)
)

(defun get-tile-is-wall (pos)
    (eq (cdr (assoc pos field :test #'equal)) :wall)
)

(defun get-new-position (pos direction)
    (let* (
        (newtile (get-target-tile pos direction)) 
        (newpos (car newtile))
        (newdir (cadr newtile))
    )
    (if (get-tile-is-wall newpos)
        (list pos direction)
        newtile
    )
    )
)


(defun rotate-right (direction)
    (ecase direction
        (:right :down)
        (:down :left)
        (:left :up)
        (:up :right)
    )
)

(defun tile-to-real (pos)
    (let* (
        (tile (car pos))
        (x (cadr pos))
        (y (caddr pos))
        (r (car (rassoc tile tiles)))
        (rx (car r))
        (ry (cadr r))
    )
        (list (+ (* rx tilesize) x) (+ (* ry tilesize) y))
    )
)

(defun walk ()
    (setf pos (list :A 0 0))
    (setf dir :right)
    (loop for dist in instructions do
        (loop for index from 1 to dist do
            ;;(pdebug (list pos dir))
            (setf tmp (get-new-position pos dir))
            (setf pos (car tmp))
            (setf dir (cadr tmp))
            (setf rpos (tile-to-real pos))
            (setf fieldmap (acons rpos (ecase dir (:up #\^) (:down #\v) (:left #\<) (:right #\>)) fieldmap))
        )
        (setf dir (rotate-right dir))
        ;(pdebug dir)
    )
    (loop for y from 0 to (- yloop 1) do 
        (loop for x from 0 to (- xloop 1) do 
            (let (
                (charac (cdr (assoc (list x y) fieldmap :test #'equal)))
            )
                (format t "~a" (if (null charac) #\  charac))
            )
        )
        (terpri)
    )
    (list rpos dir)
)


(defun value (pos direction)
    (+
        (* (car pos) 4)
        (* (cadr pos) 1000)
        1004
        (ecase direction
            (:right 3)
            (:down 0)
            (:left 1)
            (:up 2)
        )
    )
)


(sol nil (apply #'value (walk)))



