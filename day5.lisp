(load "aoc.lisp")
(ql:quickload :split-sequence)


(defun stacks-input ()
    (car (split-sequence:split-sequence "" (load-input 5) :test #'equal))
)

(defun transform-row-of-crates (string)
    (if (< (length string) 4) (list (char string 1)) (cons (char string 1) (transform-row-of-crates (subseq string 4 nil))))
)

(defun transpose (seq)
    (if (null (car seq)) nil (cons (mapcar #'car seq) (transpose (mapcar #'cdr seq))))
)

(defun clean-spaces (seq)
    (if (char= (car seq) #\Space) (clean-spaces (cdr seq)) seq)
)

(defun load-stacks ()
    (cons nil (mapcar #'clean-spaces (transpose (mapcar #'transform-row-of-crates (stacks-input)))))
)

(defun stack-pop (seq index)
    (if (= index 0) (cons (cdar seq) (cdr seq)) (cons (car seq) (stack-pop (cdr seq) (- index 1))))
)

(defun stack-top (seq index)
    (car (nth index seq))
)

(defun stack-drop (seq index value)
    (if (= index 0) (cons (cons value (car seq)) (cdr seq)) (cons (car seq) (stack-drop (cdr seq) (- index 1) value)))
)


(defun stack-move (seq source target)
    (stack-pop (stack-drop seq target (stack-top seq source)) source)
)

(defun duplicate (seq repeat)
    (if (= repeat 0) nil (cons seq (duplicate seq (- repeat 1))))
)

(defun breakdown-moves (seq)
    (duplicate (cdr seq) (car seq))
)

(defun perform-moves (stacks moves)
    (if (null moves) stacks (stack-move (perform-moves stacks (cdr moves)) (caar moves) (cadar moves)))
)

(defun moves-input ()
    (cadr (split-sequence:split-sequence "" (load-input 5) :test #'equal))
)

(defun extract-moves-1 ()
    (reverse (mapcan #'breakdown-moves (mapcar #'extract-numbers (moves-input))))
)

(defun extract-moves-2 ()
    (reverse (mapcan #'breakdown-moves (mapcan #'convert-9000-to-9001 (mapcar #'extract-numbers (moves-input)))))
)


(defun convert-9000-to-9001 (moves)
    (list (list (car moves) (cadr moves) 0) (list (car moves) 0 (caddr moves)))
)

(defun list-to-string (lst)
    (format nil "~{~A~}" lst)
)

(defun top-level-stacks (stacks)
    (list-to-string (cdr (mapcar #'car stacks)))
)

;;(format t "~S" (extract-moves-1))
(terpri)
(format t "part 1: ~S" (top-level-stacks (perform-moves (load-stacks) (extract-moves-1))))
(terpri)
(format t "part 2: ~S" (top-level-stacks (perform-moves (load-stacks) (extract-moves-2))))
