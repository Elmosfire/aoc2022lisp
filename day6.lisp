(load "aoc.lisp")


(defun scan-subseqs (len seq)
    (if (< (length seq) len) nil (cons (subseq seq 0 len) (scan-subseqs len (cdr seq))))
)

(defun is-in (element seq eqf)
    (some #'(lambda (x) (funcall eqf x element)) seq)
)

(defun all-unique (seq eqf)
    (if (null (cdr seq)) t (and (not (is-in (car seq) (cdr seq) eqf)) (all-unique (cdr seq) eqf)))
)

(defun all-unique-char (seq)
    (all-unique seq #'char=)
)

(defun first-truth (seq)
    (if (car seq) 0 (+ 1 (first-truth (cdr seq))))
)

(defun solve (len)
    (+ len (first-truth (mapcar #'all-unique-char (scan-subseqs len (to-char-list (car (load-input 6)))))))
)

(format t "part 1:~S" (solve 4))
(terpri)
(format t "part 2:~S" (solve 14))