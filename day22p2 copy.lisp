(load "aoc.lisp")

(defun parse-map-tile (tile)
    (cdr (assoc tile (list (cons #\  nil) (cons #\. :open) (cons #\# :wall)) :test #'char=))
)


(defparameter fieldmap (build-2d-map (reverse (cdr (reverse (load-input 22))))))
(defparameter field (mapcar #'(lambda (x) (cons (car x) (parse-map-tile (cdr x)))) fieldmap))

(defparameter xloop (+ (reduce #'max (mapcar #'caar field)) 1))
(defparameter yloop (+ (reduce #'max (mapcar #'cadar field)) 1))

(pdebug (list xloop yloop))

(defun parse-movement-instruction (instruction)
    (if (string= instruction "L")
        (list 0 0)
        (if (string= instruction "R")
            nil
            (list (parse-integer instruction))
        )
    )
)



(defparameter instructions (mapcan #'parse-movement-instruction (cl-ppcre:all-matches-as-strings "([LR]|[0-9]+)" (car (reverse (load-input 22))))))

(pdebug instructions)

(defun next-pos-ignore-walls (pos direction)
    (let (
        (x (car pos))
        (y (cadr pos))
    )
    (ecase direction
        (:left (list (- x 1) y))
        (:right (list (+ x 1) y))
        (:up (list x (- y 1)))
        (:down (list x (+ y 1)))
    )
    )
)

(defun get-tile-value (x y)
    (cdr (assoc (list x y) field :test #'equal))
)

(defun find-rot-center-vert (x)
    (loop for y from 0 to (- yloop 1) do 
        (if (null (get-tile-value x y)) nil (list (list x y)))
    )
)

(defun find-rot-center-horiz (y)
    (loop for x from 0 to (- xloop 1) do 
        (if (null (get-tile-value x y)) nil (list (list x y)))
    )
)


(defun find-rot-center (pos direction)
    (ecase direction
        (:left (find-rot-center-vert (car pos)))
        (:right (find-rot-center-vert (car pos)))
        (:up (find-rot-center-horiz (cadr pos)))
        (:down (find-rot-center-horiz (cadr pos)))
    )
)



(defun loop-position (pos)
    (let (
        (x (car pos))
        (y (cadr pos))
    )
        (list (mod x xloop) (mod y yloop))
    )
)

(defun rotate-right (direction)
    (ecase direction
        (:right :down)
        (:down :left)
        (:left :up)
        (:up :right)
    )
)

(defun next-tile-loop (pos direction)
    (loop-position (next-pos-ignore-walls pos direction))
)

(defun next-tile-wrap (pos direction)
    (let (
        (nexttile (next-tile-loop pos direction))
    )
    (if (null (cdr (assoc nexttile field :test #'equal)))
        (next-tile-wrap nexttile direction)
        nexttile
    )
    )
)

(defun next-tile-wall (pos direction)
    (let (
        (nexttile (next-tile-wrap pos direction))
    )
    (if (eq (cdr (assoc nexttile field :test #'equal)) :wall)
        pos
        nexttile
    )
    )
)

(defun start-pos ()
    (next-tile-wrap (list 0 0) :right)
)

(defun walk ()
    (setf pos (start-pos))
    (setf dir :right)
    (loop for dist in instructions do
        (loop for index from 1 to dist do
            (setf pos (next-tile-wall pos dir))
            ;;(pdebug pos)
            (setf fieldmap (acons pos (ecase dir (:up #\^) (:down #\v) (:left #\<) (:right #\>)) fieldmap))
        )
        (setf dir (rotate-right dir))
        ;(pdebug dir)
    )
    (loop for y from 0 to (- yloop 1) do 
        (loop for x from 0 to (- xloop 1) do 
            (let (
                (charac (cdr (assoc (list x y) fieldmap :test #'equal)))
            )
                (format t "~a" (if (null charac) #\  charac))
            )
        )
        (terpri)
    )
    (list pos dir)
)



(defun value (pos direction)
    (+
        (* (car pos) 4)
        (* (cadr pos) 1000)
        1004
        (ecase direction
            (:right 3)
            (:down 0)
            (:left 1)
            (:up 2)
        )
    )
)


(sol (apply #'value (walk)) nil)