
(load "aoc.lisp")
(ql:quickload :cl-heap)

(defparameter *queue* (make-instance 'cl-heap:priority-queue))
(defparameter visited-set (make-hash-table))

(defun check-state-visited (state hashfunc)
    (let (
        (un (apply hashfunc (list state)))
    )
    (if (gethash un visited-set)
        nil
        (progn
            (setf (gethash un visited-set) t)
            t
        )
    )
    )
)

(defun store-state (state hashfunc heuristic)
    ;;(format t "store: ~a" state)
    (when (check-state-visited state hashfunc)
        (progn
            (cl-heap:enqueue *queue* state (apply heuristic (list state)))
        )
    )
)


(defun d-star (startstates hashfunc heuristic solcheck next)
    (setq best-remaining (apply heuristic (list (car startstates))))
    (cl-heap:empty-queue *queue*)
    (loop for startstate in startstates do
        (store-state startstate hashfunc heuristic)
    )
    (loop while t do
        (let (
            (top (cl-heap:dequeue *queue*))
        )
        (progn
        ;;(pdebug top)
        (if (null top)
            (return-from d-star nil)
            (let (
                (topheur (apply heuristic (list top)))
            )
            (if (apply solcheck (list top))
                (progn 
                    (format t "~a" top)
                    (terpri)
                    (return-from d-star top)
                )
                (progn
                    (when (< topheur best-remaining)
                    (progn
                        (setq best-remaining topheur)
                        (format t "current-heuristic: ~a" best-remaining)
                        (terpri)
                    )
                    )
                    (loop for new in (apply next (list top)) do 
                        (store-state new hashfunc heuristic)
                    )
                )
            )
            )
        )))
        
    )
)