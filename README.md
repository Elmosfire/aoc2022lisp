# aoc2022lisp

Name your input file "input1.txt" (replace number for other days) and name your tet file (with the example input) "test1.txt"
To run your day, run
```
clisp day1.lisp
```
To test your day run
```
clisp day1.lisp test
```