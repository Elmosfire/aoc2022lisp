(load "vec2d.lisp")



(defstruct sensor
    loc
    distance
)

(defun sensors-from-input-line (line)
    (let* (
        (data (%from-string line))
        (loc (car data))
        (target (cadr data))
    )
    (make-sensor
        :loc loc
        :distance (%manhattan target loc)
    )
    )
)

(defun beacons-from-input-line (line)
    (cadr (%from-string line))
)

(setq sensors (mapcar #'sensors-from-input-line (load-input 15)))
(setq beacons (remove-duplicates (mapcar #'beacons-from-input-line (load-input 15)) :test #'%=))

(defun left-bound (sensor)
    (- (%x (sensor-loc sensor)) (sensor-distance sensor)) 
)

(defun right-bound (sensor)
    (+ (%x (sensor-loc sensor)) (sensor-distance sensor)) 
)

(defun lowest-x ()
    (reduce #'min (mapcar #'left-bound sensors))
)

(defun greatest-x ()
    (reduce #'max (mapcar #'right-bound sensors))
)

(defun tile-close-to-sensor (loc sensor)
    (<= (%manhattan loc (sensor-loc sensor)) (sensor-distance sensor))
)

(defun tile-close-to-any-sensor (loc)
    (some #'(lambda (x) x) (mapcar #'(lambda (x) (tile-close-to-sensor loc x)) sensors))
)

(defun tile-is-on-beacon (loc)
    (some #'(lambda (x) (%= loc x)) beacons)
)

(defun tile-cannot-contain-distress (loc)
    (and (tile-close-to-any-sensor loc) (not (tile-is-on-beacon loc)))
)

(defun tiles-in-row-ten ()
    (loop for x from (lowest-x) to (greatest-x) collect (% x 10))
)

(defun bool-to-int (bool)
    (if bool 1 0)
)

(defun project-to-h-line (sensor newy)
    (let* (
        (x (%x (sensor-loc sensor)))
        (y (%y (sensor-loc sensor)))
        (d (sensor-distance sensor))
        (s (abs (- y newy)))
    )
    (if (>= (- d s) 0)
    (list
        (make-sensor
            :loc (% x newy)
            :distance
            (- d s)
        )
    )
    nil
    )
    )
)

(defun interval (sensor)
    (list (left-bound sensor) (right-bound sensor))
)

(defun intervals (newy)
    (mapcar #'interval (mapcan #'(lambda (x) (project-to-h-line x newy)) sensors))
)

(defun comp-int (vec1 vec2)
    (< (car vec1) (car vec2))
)

(defun overlap-or-border (i1 i2)
    (let* (
        (right1 (cadr i1))
        (left2 (car i2))
    )
    (>= (+ right1 1) left2)
    )
)

(defun fuse (i1 i2)
    (let* (
        (left1 (car i1))
        (right1 (cadr i1))
        (right2 (cadr i2))
    )
    (list left1 (max right1 right2))
    )
)

(defun combine (lst)
    (if (>= (length lst) 2)
    (let* (
        (head1 (car lst))
        (head2 (cadr lst))
        (tail (cddr lst))
    )
    (if (overlap-or-border head1 head2)
        (combine (cons (fuse head1 head2) tail))
        (cons head1 (combine (cons head2 tail)))
    )
    )
    lst
    )
)

(defun interval-len (interval)
    (+ (- (cadr interval) (car interval)) 1)
)

(defun sum-interval-len (intervals)
    (reduce #'+ (mapcar #'interval-len intervals))
)

(defun count-beacons-at-line (ny)
    (reduce #'+ (mapcar #'(lambda (x) (if (= (%y x) ny) 1 0)) beacons))
)

(defun count-line (ny)
    (-
        (sum-interval-len (combine (sort (intervals ny) #'comp-int)))
        (count-beacons-at-line ny)
    )
)

(defun clip-interval-right (interval maxx)
    (let (
        (left (car interval))
        (right (cadr interval))
    )
    (if (> left maxx)
        nil
    (if (> right maxx)
        (list (list left maxx))
        (list interval)
    )
    )
    )
)

(defun clip-interval-left (interval)
    (let (
        (left (car interval))
        (right (cadr interval))
    )
    (if (< right 0)
        nil
    (if (< left 0)
        (list (list 0 right))
        (list interval)
    )
    )
    )
)
(defun clip-interval-both (interval maxx)
    (mapcan #'clip-interval-left (clip-interval-right interval maxx))
)

(defun clip-list (intervals maxx)
    (mapcan #'(lambda (x) (clip-interval-both x maxx)) intervals)
)

(defun extract-nonfull (intervalset)
    (let (
        (second (cdr intervalset))
    )
    (if (null second)
        nil
        (caar second)
    )
    )
)

(defun extract ()
    (loop for y from 0 to 4000000 do
        (progn
            (if (= (rem y 10000) 0) (pdebug y))
            (let(
                (val (extract-nonfull (combine (clip-list (sort (intervals y) #'comp-int) 4000000))))
            )
            (when (not (null val)) (return-from extract (list (- val 1) y (+ (* (- val 1) 4000000) y))))
            )
        )
    )
)

(pdebug (extract))

