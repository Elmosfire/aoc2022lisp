(load "~/quicklisp/setup.lisp")
(ql:quickload :cl-ppcre)

(setq global-aoc-arg0 (format nil "~a" (car *args*)))

(format t (if (string= global-aoc-arg0 "test") "testing mode" "real mode"))
(terpri)

(defun load-input (day)
    (with-open-file (stream (format nil (if (string= global-aoc-arg0 "test") "test~a.txt" "input~a.txt") day))
        (loop for line = (read-line stream nil)
            while line
            collect line
        )
    )
)

(defun extract-numbers (string)
    (mapcar #'parse-integer (cl-ppcre:all-matches-as-strings (cl-ppcre:parse-string "-?[0-9]+") string))
)

(defun extract-words (string)
    (cl-ppcre:all-matches-as-strings (cl-ppcre:parse-string "[a-zA-Z0-9]+") string)
)

(defun to-char-list (string)
    (if (= (length string) 0) nil (cons (char string 0) (to-char-list (subseq string 1 nil))))
)

(defun char-index (start index)
    "calculate the relative asscii index from start to index"
    (-  (char-code index) (char-code start)) 
)

(defun mapcaar (func seq)
    (mapcar #'(lambda (x) (mapcar func x)) seq)
)

(defun contains (item sequence) 
    (if (member item sequence) T NIL)
)


(defun pdebug (object)
    (progn
        (format t ":: ~S" object)
        (terpri)
    )
    
)

(defun wdebug (name object)
    (progn
        (format t "~a=~S" name object)
        (terpri)
        object
    )
)

(defun sol (p1 p2)
    (progn
        (format t "part1: ~S" p1)
        (terpri)
        (format t "part2: ~S" p2)
        (terpri)
    )
)


(defun range (mn mx)
   (loop for n from mn below mx by 1
      collect n))

(defun curry (fn &rest args)
  (lambda (&rest remaining-args)
    (apply fn (append args remaining-args))))

(defun cumulative-reduce (func seq init)
    "reduce but returns a list of temp results"
    (if (null seq) (list init)
        (cons 
            init 
            (cumulative-reduce func (cdr seq) (apply func (list (car seq) init)))
        )
    )
)

(defun enumerate (seq)
    (loop for index from 0 to (- (length seq) 1)
        collect (list index (nth index seq))
    )
)

(defun enumerate-alist (seq)
    (loop for index from 0 to (- (length seq) 1)
        collect (cons index (nth index seq))
    )
)

(defun enumerate-alist-for-hd (seq)
    (loop for index from 0 to (- (length seq) 1)
        collect (cons (list index) (nth index seq))
    )
)

(defun expand-enumerate-helper (key value)
    (loop for index from 0 to (- (length value) 1)
        collect (cons (cons index key) (nth index value))
    )
)

(defun expand-enumerate-2d (seq)
    (mapcan #'(lambda (x) (expand-enumerate-helper (car x) (cdr x))) (enumerate-alist-for-hd seq))
)

(defun build-2d-map (seq)
    (expand-enumerate-2d (mapcar #'to-char-list seq))
)




(defun charlist-to-string (lst)
    (apply #'concatenate (cons 'string (mapcar 'string lst)))
)