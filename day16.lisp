(load "aoc.lisp")
(ql:quickload :cl-heap)

(defun hash-char-list (lst)
    (if (null lst)
        0
        (+ (char-index #\A (car lst)) (* 29 (hash-char-list (cdr lst))))
    )
)

(defun hash-string (string)
    (+ (hash-char-list (to-char-list string)) 171865)
)


(defun extract-valve (string)
    (cl-ppcre:all-matches-as-strings (cl-ppcre:parse-string "([A-Z][A-Z]|[0-9]+)") string)
)

(defstruct valve
    name
    rate
    neighbors
)

(defun value-from-string (line)
    (let* (
        (args (extract-valve line))
        (name (car args))
        (rate (parse-integer (cadr args)))
        (neighbors (cddr args))
    )
    (cons name (make-valve
        :name name
        :rate rate
        :neighbors neighbors
    ))
    )
)

(setq valves (mapcar #'value-from-string (load-input 16)))

(setq validvalves (remove nil (loop for x in valves collect (if (> (valve-rate (cdr x)) 0) x nil))))


(defun find-valve (name)
    (cdr (assoc name valves :test #'string=))
)



(defun ρ (name)
    (valve-rate (find-valve name))
)

(defun neighbors (name)
    (valve-neighbors (find-valve name))
)

(defun valve-pair-hash (v1 v2)
    (* (hash-string v1) (hash-string v2))
)

(setf distances (make-hash-table :test #'eq))

(defun store-distance (v1 v2 distance)
    (when (null (gethash (valve-pair-hash v1 v2) distances))
        (setf (gethash (valve-pair-hash v1 v2) distances) distance)
    )
)

(loop for v1 in (mapcar #'car valves) do
    (loop for v2 in (neighbors v1) do
        (store-distance v1 v2 1)
    )
    (store-distance v1 v1 0)
)

(loop for dst from 1 to 100 do
    (loop for v1 in (mapcar #'car valves) do
        (loop for v2 in (mapcar #'car valves) do
            (when (= (gethash (valve-pair-hash v1 v2) distances 0) dst)
                (loop for v3 in (neighbors v2) do
                    (store-distance v1 v3 (+ dst 1))
                )
            )
        )
    )
)

(defun η (v1 v2)
    (gethash (valve-pair-hash v1 v2) distances)
)



(defstruct vstate
    current
    elephant
    opened
    score
    remaining
    remaining-elephant
    history
)

(defun start-state (time- time-e)
    (make-vstate
        :current "AA"
        :elephant "AA"
        :opened nil
        :score 0
        :remaining time-
        :remaining-elephant time-e
        :history nil
    )
)



(defun can-open-current (state)
    (and (null (find (vstate-current state) (vstate-opened state) :test #'string=)) (> (ρ (vstate-current state)) 0))
)

(defun can-open-current-elephant (state)
    (and (null (find (vstate-elephant state) (vstate-opened state) :test #'string=)) (> (ρ (vstate-current state)) 0))
)


(defun move-and-open (state new)
    (let* (
        (current (vstate-current state))
        (elephant (vstate-elephant state))
        (opened (vstate-opened state))
        (score (vstate-score state))
        (remaining (vstate-remaining state))
        (remaining-elephant (vstate-remaining-elephant state))
        (history (vstate-history state))

        (open-time (- remaining (η current new)))
        (new-remaining (- open-time 1))
        (target-score (* (ρ new) new-remaining))

        (valid (>= new-remaining 0))

    )
        (if valid
        (make-vstate
            :current new
            :elephant elephant
            :opened (cons new opened)
            :score (+ score target-score)
            :remaining new-remaining
            :remaining-elephant remaining-elephant
            :history (cons (list :goto new (η current new) new-remaining) history)
        )
        nil
        )
    )
)

(defun move-and-open-e (state new)
    (let* (
        (current (vstate-current state))
        (elephant (vstate-elephant state))
        (opened (vstate-opened state))
        (score (vstate-score state))
        (remaining (vstate-remaining state))
        (remaining-elephant (vstate-remaining-elephant state))
        (history (vstate-history state))

        (open-time (- remaining-elephant (η elephant new)))
        (new-remaining (- open-time 1))
        (target-score (* (ρ new) new-remaining))

        (valid (>= new-remaining 0))

    )
        (if valid
        (make-vstate
            :current current
            :elephant new
            :opened (cons new opened)
            :score (+ score target-score)
            :remaining remaining
            :remaining-elephant new-remaining
            :history (cons (list :goto-e new (η elephant new) new-remaining) history)
        )
        nil
        )
    )
)

(defun finalise (state)
    (let* (
        (current (vstate-current state))
        (elephant (vstate-elephant state))
        (opened (vstate-opened state))
        (score (vstate-score state))
        (remaining (vstate-remaining state))
        (remaining-elephant (vstate-remaining-elephant state))
        (history (vstate-history state))


    )
        
        (make-vstate
            :current current
            :elephant elephant
            :opened opened
            :score score
            :remaining 0
            :remaining-elephant (if (= remaining 0) 0 remaining-elephant)
            :history (cons (list :finalise remaining remaining-elephant) history)
        )
        
    )
)




(defun next-moved (state)
    (remove nil (loop for x in (closed-valid-valves state) collect (move-and-open state x)))
)

(defun next-moved-elephant (state)
    (remove nil (loop for x in (closed-valid-valves state) collect (move-and-open-e state x)))
)

(defun next (state)
    (cons (finalise state) (if (> (vstate-remaining state) 0) (next-moved state) (next-moved-elephant state)))
)

(defun closed-valves (state)
    (mapcan #'(
        lambda (v) 
        (if (null (find v (vstate-opened state) :test #'string=))
            (list v)
            nil
        )
    )
        (loop for valve in valves collect (car valve)) 
    )
)

(defun closed-valid-valves (state)
    (mapcan #'(
        lambda (v) 
        (if (null (find v (vstate-opened state) :test #'string=))
            (list v)
            nil
        )
    )
        (loop for valve in validvalves collect (car valve)) 
    )
)

(defun best-rates (valves)
    (sort (copy-seq (loop for x in valves collect (ρ x))) #'>)
)

(defun best-rates-combined (valves remaining)
    (if (<= remaining 0) 
        0
    (if (null valves)
        0
        (+ (* (car valves) remaining) (best-rates-combined (cdr valves) (- remaining 2)))
    ))
)

(defun max-to-reach-score (st)
    (+ 
        (best-rates-combined (best-rates (closed-valves st)) (vstate-remaining st))
        (best-rates-combined (best-rates (closed-valves st)) (vstate-remaining-elephant st))
    )
)

(defun heuristic_old (st)
    (- (+ (vstate-score st) (max-to-reach-score st)))
)

(defun maximum-valve-value (st valve)
    (let (
        (open-time (- (vstate-remaining st) (η (vstate-current st) valve)))
    )
    (* (+ open-time 1) (ρ valve))
    )
)

(defun maximum-elephant-value (st valve)
    (let (
        (open-time (- (vstate-remaining-elephant st) (η (vstate-elephant st) valve)))
    )
    (* (+ open-time 1) (ρ valve))
    )
)

(defun heuristic (st)
    (- (+ (vstate-score st) (reduce #'+ (loop for valve in (closed-valid-valves st) collect (+ (maximum-valve-value st valve) (maximum-elephant-value st valve))))))
)

(defparameter *queue* (make-instance 'cl-heap:priority-queue))





(setq visited-set (make-hash-table :test #'eq))


(defun unique-identifier (state)
    (+
        (* 1 (hash-string (vstate-current state)))
        (* 7 (hash-string (vstate-elephant state)))
        (* 107 (reduce #'* (mapcar #'hash-string (vstate-opened state))))
    )
)

(defun check-state-visited (state)
    (let (
        (un (unique-identifier state))
    )
    (if (gethash un visited-set)
        nil
        (progn
            (setf (gethash un visited-set) t)
            t
        )
    )
    )
)

(defun store-state (state)
    (when (check-state-visited state)
        (progn

            (cl-heap:enqueue *queue* state (heuristic state))
        )
    )
)

(defun all-valves-open (st)
    (= (length valves) (length (vstate-opened st)))
)

(defun totremaining (state)
    (+ (vstate-remaining state) (vstate-remaining-elephant state))
)

(defun search-better-state (mx mxe)
    (setq best-remaining (+ mx mxe))
    (store-state (start-state mx mxe))
    (loop while t do
        (let (
            (top (cl-heap:dequeue *queue*))
        )
        (if (null top)
            (return-from search-better-state nil)
            (if (and (not (all-valves-open top)) (> (totremaining top) 0))
                (progn
                    ;;(format t ">> ~a ~a" (vstate-score top) (heuristic top))
                    ;;(terpri)
                    (when (< (totremaining top) best-remaining)
                    (progn
                        (setq best-remaining (totremaining top))
                        (format t "current: ~a ~a ~a ~a" best-remaining (vstate-score top) (heuristic top) (vstate-history top))
                        (terpri)
                    )
                    )
                    (loop for new in (next top) do (store-state new))
                )
                (progn 
                    (format t "~a ~a" (vstate-score top) (heuristic top))
                    (terpri)
                    (return-from search-better-state nil)
                )
            )
        )
        )
    )
)




;;(search-better-state 30 0)
(search-better-state 26 26)
