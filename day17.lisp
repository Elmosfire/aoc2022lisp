(load "aoc.lisp")


(setq rocks-shapes (list
    (list 
        (list nil nil t   t   t   t   nil)
    )
    (list 
        (list nil nil nil t   nil nil nil)
        (list nil nil t   t   t   nil nil)
        (list nil nil nil t   nil nil nil)
    )
    (list 
        (list nil nil nil nil t   nil nil)
        (list nil nil nil nil t   nil nil)
        (list nil nil t   t   t   nil nil)
    )
    (list 
        (list nil nil t   nil nil nil nil)
        (list nil nil t   nil nil nil nil)
        (list nil nil t   nil nil nil nil)
        (list nil nil t   nil nil nil nil)
    )
    (list 
        (list nil nil t   t   nil nil nil)
        (list nil nil t   t   nil nil nil)
    )
))

(setq rocks-index 0)


(defun create-gap (freelayers)
    (loop for x from 1 to freelayers collect (list nil nil nil nil nil nil nil))
)

(defun create-rocks (index freelayers)
    (reduce #'cons (nth (rem index (length rocks-shapes)) rocks-shapes) :initial-value (create-gap freelayers) :from-end t)
)

(defun normalise-rocks (rocks target-height)
    (let (
        (freelayers (max 0 (- target-height (length rocks))))
    )
    (reduce #'cons (create-gap freelayers) :initial-value rocks :from-end t)
    )
)

(defun print-rocks (rocks)
    (loop for line in rocks do
        (format t "~a"  #\|)
        (loop for tile in line do
            (format t "~a" (if tile #\█ #\. ))
            
        )
        (format t "~a"  #\|)
        (terpri)
    )
    (format t "~a" "+-------+")
    (terpri)
)

(defun can-move-left (rocks)
    (not (some 'car rocks))
)

(defun eltlast (lst)
    (car (last lst))
)

(defun can-move-right (rocks)
    (not (some 'eltlast rocks))
)

(defun can-move-down (rocks)
    (not (car (some 'identity (last rocks))))
)

(defun move-left (rocks)
    (loop for line in rocks collect
        (reduce #'cons (cdr line) :initial-value (list nil) :from-end t)
    )
)

(defun move-right  (rocks)
    (loop for line in rocks collect
        (cons nil (loop for x in line for y in (cdr line) collect x))
    )
)

(defun try-move (direction rocks)
    (ecase direction
        (:left (if (can-move-left rocks) (move-left rocks) rocks))
        (:right (if (can-move-right rocks) (move-right rocks) rocks))
    )
)

(defun collect-input ()
    (loop for x in (to-char-list (mapcan 'identity (load-input 17))) collect (if (char= x #\<) :left (if (char= x #\>) :right nil)))
)

(defun move-down (rocks)
    (cons (list nil nil nil nil nil nil nil) (loop for x in rocks for y in (cdr rocks) collect x))
)

(defun overlap (a b)
    (progn
        (loop for line-a in a for line-b in b do
            (loop for tile-a in line-a for tile-b in line-b do
                (when (and tile-a tile-b) (return-from overlap t))
            )
        )
        nil
    )
)


(setf cave (list (list t t t t t t t)))

(setf falling nil)

(defun simplify (rocks)
    (if (every #'not (car rocks)) (simplify (cdr rocks)) rocks)
)

(defun normalise ()
    (let* (
        (newcave (simplify cave))
        (newfalling (simplify falling))
        (newlen (max (length newcave) (length newfalling)))
    )
        (progn
        (setf cave (normalise-rocks newcave newlen))
        (setf falling (normalise-rocks newfalling newlen))
        )
    )
)



(defun print-rocks-combined (rocks1 rocks2)
    (loop for line1 in rocks1 for line2 in rocks2 do
        (format t "~a"  #\|)
        (loop for tile1 in line1 for tile2 in line2 do
            (format t "~a" (if tile1 #\█ (if tile2 #\@ #\ )))
            
        )
        (format t "~a"  #\|)
        (terpri)
    )
    (format t "~a" "+-------+")
    (terpri)
)

(defun combine (rocks1 rocks2)
    (loop for line1 in rocks1 for line2 in rocks2 collect
        (loop for tile1 in line1 for tile2 in line2 collect
            (or tile1 tile2)
        )
    )
)





(setq jets-list (remove nil (collect-input)))
(setq jets-index 0)

(defun pop-jet ()
    (progn 
        (setq jets-index (+ jets-index 1))
        (nth (rem (- jets-index 1) (length jets-list)) jets-list)
    )
)

(defun pop-rock (freelayers)
    (progn 
        (setq rocks-index (+ rocks-index 1))
        (create-rocks (- rocks-index 1) freelayers)
    )
)

(defun new-rock ()
    (progn
        (setf falling (pop-rock (+ (length cave) 3)))
        (normalise)
        
    )
)

(defun simplify-overlap (sys c)
    (let* (
        (newcave (simplify c))
        (newfalling (simplify sys))
        (newlen (min (length newcave) (length newfalling)))
        (newstart-cave (- (length cave) newlen))
        (newstart-falling (- (length falling) newlen))
        (sub-cave (subseq cave newstart-cave (+ newstart-cave 6)))
        (sub-falling (subseq cave newstart-falling (+ newstart-falling 6)))
    )
        (overlap sub-cave sub-falling)
    )
)

(defun move-tmp (newsys)
    (if (overlap  newsys cave) 
        (progn
            ;;(pdebug "overlap")
            t    
        )
        (progn
            (setf falling newsys)
            ;;(pdebug "moved")
            nil
        )
    )
)

(defun move-by-jet ()
    (move-tmp (try-move (pop-jet) falling))
)

(defun settle-rock ()
    (setf cave (combine cave falling))
)

(setq history (make-hash-table))
(setq history-store (make-hash-table))
(setq history-list (list nil))

(defun move-by-gravity ()
    (if (move-tmp (move-down falling))
        (progn
            (normalise)
            (settle-rock)
            (push (- (length (simplify cave)) 1) history-list)
            (when (= (rem rocks-index 5 ) 0)
                (let (
                    (rel-jet-index (rem jets-index (length jets-list)))
                )
                (progn
                    (format t "~a ~a ~a" (rem jets-index (length jets-list)) rocks-index (- (length (simplify cave)) 1))
                    (if (null (gethash rel-jet-index history-store))
                        (setf (gethash rel-jet-index history-store) rocks-index)
                        (if (null (gethash rel-jet-index history))
                            (setf (gethash rel-jet-index history) rocks-index)
                            (return-from move-by-gravity (list (gethash rel-jet-index history) rocks-index))
                    )
                    )
                    (terpri)
                )
                )
            )
            
            (new-rock)
        )
    )
    (normalise)
    nil
)


(new-rock)
(defun run ()
    (loop while (<= rocks-index 1000000000000) do
        (move-by-jet)
        (setf data (move-by-gravity))
        (when (not (null data))
            (return-from run data)
        )
        ;;(print-rocks-combined cave falling)
    )
)

(let* (
    (r (run))
    (loc1 (car r))
    (loc2 (cadr r))
    (modbase (- loc2 loc1))
    (target 1000000000000)
    (offset (- target loc1))
    (moffset (rem offset modbase))
    (repoffset (floor offset modbase))
    (hlist (reverse history-list))
    (grow (- (nth loc2 hlist) (nth loc1 hlist)))
    (base (nth (+ moffset loc1) hlist))
)
    (pdebug (+ moffset loc1))
    (pdebug (+ base (* grow repoffset)))
)

