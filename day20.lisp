(load "aoc.lisp")

(defun start ()
    (enumerate-alist (mapcar #'parse-integer (load-input 20)))
)

(defun start2 ()
    (enumerate-alist (mapcar #'(lambda (x) (* x 811589153)) (mapcar #'parse-integer (load-input 20))))
)

(defun rotate-list (seq amount) 
    (let* (
        (l (length seq))
        (r (mod amount l))
    )
    (reduce #'cons (subseq seq r l) :initial-value (subseq seq 0 r) :from-end t)
    )
)

(defun rotate-list-not-first (seq amount)
    (let* (
        (head (car seq))
        (tail (cdr seq))
    )
    (cons head (rotate-list tail amount))
    )
)

(defun reveal (seq marker)
    (rotate-list seq (position-if #'(lambda (x) (= (car x) marker)) seq))
)

(defun shift-marker (seq marker)
    (let (
        (shifted (reveal seq marker))
    )
    (rotate-list-not-first shifted (cdar shifted))
    )
)



(defun reveal0 (seq)
    (rotate-list seq (position-if #'(lambda (x) (= (cdr x) 0)) seq))
)


(setf state (start2))

(loop for index from 0 to 9 do
    (loop for index from 0 to (- (length state) 1) do
        (setf state (shift-marker state index))
        ;;(pdebug (mapcar #'cdr state))
        ;;(pdebug state)
    )
)

(defun nth-after-0 (state n)
    (cdar (rotate-list (reveal0 state) n))
)


(pdebug ( + (nth-after-0 state 1000) (nth-after-0 state 2000) (nth-after-0 state 3000)))

