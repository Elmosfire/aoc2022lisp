(load "aoc.lisp")

(defun char-index (start index)
    "calculate the relative asscii index from start to index"
    (-  (char-code index) (char-code start)) 
)

(defun digit-to-num (charval)
    (+ (char-index #\0 charval) 1)
)

(defun to-num-list (seq)
    (mapcar #'digit-to-num (to-char-list seq))
)

(defun load-grid ()
    (mapcar #'to-num-list (load-input 8))
)

(defun transpose (list-of-lists)
  (apply #'mapcar #'list list-of-lists)
)

(defun reflections (seq)
    (list seq (mapcar #'reverse seq) (transpose seq) (mapcar #'reverse (transpose seq))) 
)

(defun revert-reflection (sq1 sq2 sq3 sq4)
    (list sq1 (mapcar #'reverse sq2) (transpose sq3) (transpose (mapcar #'reverse  sq4))) 
)

(defun visible-line (seq)
    (if (null seq) nil
        (if (<= (car seq) 0) 
            (cons nil (visible-line (cdr seq)))
            (cons t (visible-line (mapcar #'(lambda (x) (- x (car seq))) (cdr seq))))
        )
    )
)

(defun visible-grid (grid)
    (mapcar #'visible-line grid)
)

(defun concat (l1 l2)
    (reduce #'cons l1 :initial-value l2 :from-end t) 
)

(defun flatten (grid)
    (reduce #'concat grid)
)

(defun all (seq)
    (if (null seq) t (if (car seq) (all (cdr seq)) nil))
)

(defun any (seq)
    (if (null seq) nil (if (car seq) t (any (cdr seq))))
)

(defun bool-to-num (val)
    (if val 1 0)
)


(defun vision-grid ()
    (apply #'revert-reflection (mapcar #'visible-grid (reflections (load-grid))))
)


(pdebug (apply #'revert-reflection  (reflections (load-grid))))
(pdebug (vision-grid))


(setq load-grid (load-grid))
(setq dim (length (car load-grid)))


(defun tree-local (x y grid)
    (nth x (nth y grid))
) 

(defun view-left (x y grid)
    (if (> x 0) (reverse (subseq (nth y grid) 0 x)) nil)
)

(defun view-right (x y grid)
    (subseq (nth y grid) (+ x 1) nil)
)

(defun view-count (localtree seq)
    (if (null seq) 0 (if (>= (car seq) localtree) 1 (+ 1 (view-count localtree (cdr seq)))))
)

(defun horiz-scenic-score (x y grid)
    (* (view-count (tree-local x y grid) (view-left x y grid)) (view-count (tree-local x y grid) (view-right x y grid)))
)

(defun vert-scenic-score (x y grid)
    (horiz-scenic-score y x (transpose grid))
)

(defun scenic-score (x y)
    (* (horiz-scenic-score x y load-grid) (vert-scenic-score x y load-grid)) 
)

(defun max-item (list)
  (apply #'max list))

(defun row-scenic-score (y)
    (max-item (mapcar #'(lambda (x) (scenic-score x y)) (range 0 dim)))
)

(defun max-scenic-score ()
    (max-item (mapcar #'(lambda (y) (row-scenic-score y)) (range 0 dim)))
)

(pdebug (range 0 10))
(pdebug (view-right 2 2 load-grid))

(sol (reduce #'+ (mapcar #'bool-to-num (mapcar #'any (transpose (mapcar #'flatten (vision-grid)))))) (max-scenic-score))