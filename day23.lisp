(load "aoc.lisp")







(defun filter-elf-location (tile)
    (if (char= (cdr tile) #\#) (list (car tile)) nil)
)

(defparameter startstate (mapcan #'filter-elf-location (build-2d-map (load-input 23))))

(defun movement (dir loc)
    (let (
        (x (car loc))
        (y (cadr loc))
    )
    (ecase dir
        (:0 (list x y))
        (:N (list x (- y 1)))
        (:S (list x (+ y 1)))
        (:E (list (+ x 1) y))
        (:W (list (- x 1) y))
        (:NE (list (+ x 1) (- y 1)))
        (:NW (list (- x 1) (- y 1)))
        (:SE (list (+ x 1) (+ y 1)))
        (:SW (list (- x 1) (+ y 1)))
    )
    )
)

(defun no-elf-at (dir loc allpos)
    (not (member (movement dir loc) allpos :test #'equal))
)

(defun no-elfs-in (dirs loc allpos)
    (every #'(lambda (dir) (no-elf-at dir loc allpos)) dirs)
)

(defparameter direction-list (list 
    :N (list :N :NE :NW)
    :S (list :S :SE :SW)
    :E (list :E :NE :SE)
    :W (list :W :NW :SW)
    :0 (list :N :S :E :NE :SE :W :NW :SW)
    )
)

(defparameter direction-order (list 
    :N (list :N :S :W :E)
    :S (list :S :W :E :N)
    :W (list :W :E :N :S)
    :E (list :E :N :S :W)
    )
)

(defun move-elf (loc allpos d1 d2 d3 d4)
    (if (no-elfs-in (getf direction-list :0) loc allpos)
        :0
    (if (no-elfs-in (getf direction-list d1) loc allpos)
        d1
    (if (no-elfs-in (getf direction-list d2) loc allpos)
        d2
    (if (no-elfs-in (getf direction-list d3) loc allpos)
        d3
    (if (no-elfs-in (getf direction-list d4) loc allpos)
        d4
        :0
    )))))
)

(defun move-elf-first (loc allpos d1)
    (let* (
        (dirlist (getf direction-order d1))
        (d2 (second dirlist))
        (d3 (third dirlist))
        (d4 (fourth dirlist))
    )
    (move-elf loc allpos d1 d2 d3 d4)
    )
)

(defun move-all-elves (allpos d1)
    (loop for elf in allpos collect
        (list elf (movement (move-elf-first elf allpos d1) elf))
    )
)


(defun print-state (allpos)
    (let (
        (minx (reduce #'min (mapcar #'car  allpos)))
        (miny (reduce #'min (mapcar #'cadr allpos)))
        (maxx (reduce #'max (mapcar #'car  allpos)))
        (maxy (reduce #'max (mapcar #'cadr allpos)))
    )
    (loop for y from miny to maxy do
        (loop for x from minx to maxx do
            (format t "~a" (if (no-elf-at :0 (list x y) allpos) #\. #\█))
        )
        (terpri)
    )
    (terpri)
    )
)

(defun cover-ground (allpos)
    (let (
        (minx (reduce #'min (mapcar #'car  allpos)))
        (miny (reduce #'min (mapcar #'cadr allpos)))
        (maxx (reduce #'max (mapcar #'car  allpos)))
        (maxy (reduce #'max (mapcar #'cadr allpos)))
    )
    (reduce #'+ (loop for y from miny to maxy collect
        (reduce #'+ (loop for x from minx to maxx collect
            (if (no-elf-at :0 (list x y) allpos) 1 0)
        ))
    ))
    )
)

(defun is-duplicate (pos allpos)
    (>= (count pos allpos :test #'equal) 2)
)

(defun move-all-elves-corr (allpos index)
    (let* (
        (d1 (elt (getf direction-order :N) (mod index 4)))
        (mov (move-all-elves allpos d1))
        (targets (mapcar #'cadr mov))
    )
    (loop for step- in mov collect
        (let (
            (from- (car step-))
            (to-  (cadr step-))
        )
        (if (is-duplicate to- targets) from- to-)
        )
    )
    )
)




(defun evolve (mx)
    (defparameter currentstate startstate)
    (loop for index from 0 to (- mx 1) do
        (format t "~a ~C" index #\return)
        (setf newstate (move-all-elves-corr currentstate index))
        (when (equal currentstate newstate) 
            (progn 
                (return-from evolve (list :rounds (+ index 1)))
            )
        )
        (setf currentstate newstate)
        ;;(print-state currentstate)
    )
    (list :cover (cover-ground currentstate))
)




(sol (evolve 10) (evolve 1000))
