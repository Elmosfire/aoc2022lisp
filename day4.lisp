(load "aoc.lisp")

(defun extract1 (string)
    (cl-ppcre:split "," string)
)

(defun extract2 (string)
    (mapcar #'parse-integer (cl-ppcre:split "-" string))
)

(defun extract (string)
    (mapcar #'extract2 (extract1 string))
)

(defstruct elf
   left-start 
   left-stop 
   right-start 
   right-stop
)

(defun elfpair-from-line (line)
    (make-elf :left-start (caar line) :left-stop (cadar line) :right-start (caadr line) :right-stop (cadadr line))
)

(defun contained-left (elf)
    (and (>= (elf-left-start elf) (elf-right-start elf)) (<= (elf-left-stop elf) (elf-right-stop elf)))
)

(defun contained-right (elf)
    (and (<= (elf-left-start elf) (elf-right-start elf)) (>= (elf-left-stop elf) (elf-right-stop elf)))
)

(defun collide (elf)
    (and (<= (elf-left-start elf) (elf-right-stop elf)) (<= (elf-right-start elf) (elf-left-stop elf)))
)


(defun contained (elf)
    (or (contained-left elf) (contained-right elf))
)

(defun count-true (lst)
    (if (null lst) 0  (+ (if (car lst) 1 0) (count-true (cdr lst))))
)


(format t "part1: ~a" (count-true (mapcar #'contained (mapcar #'elfpair-from-line (mapcar #'extract (load-input 4))))))
(terpri)
(format t "part2: ~a" (count-true (mapcar #'collide (mapcar #'elfpair-from-line (mapcar #'extract (load-input 4))))))