(load "aoc.lisp")
(ql:quickload :cl-heap)

(defstruct blueprint
    blueprintid
    ore-ore
    ore-clay
    ore-obsidian
    clay-obsidian
    ore-geode
    obsidian-geode
)


(defun build-blueprint (nums)
    (make-blueprint
    :blueprintid (elt nums 0)
    :ore-ore (elt nums 1)
    :ore-clay (elt nums 2) 
    :ore-obsidian (elt nums 3)
    :clay-obsidian (elt nums 4)
    :ore-geode (elt nums 5)
    :obsidian-geode (elt nums 6)
    )
)

(defun extract-cost (bp resourse bot-type)
    (ecase bot-type 
        (:ore (ecase resourse
            (:ore (blueprint-ore-ore bp))
            (:clay 0)
            (:obsidian 0)
            (:geode 0)
        ))
        (:clay (ecase resourse
            (:ore (blueprint-ore-clay bp))
            (:clay 0)
            (:obsidian 0)
            (:geode 0)
        ))
        (:obsidian (ecase resourse
            (:ore (blueprint-ore-obsidian bp))
            (:clay (blueprint-clay-obsidian bp))
            (:obsidian 0)
            (:geode 0)
        ))
        (:geode (ecase resourse
            (:ore (blueprint-ore-geode bp))
            (:clay 0)
            (:obsidian (blueprint-obsidian-geode bp))
            (:geode 0)
        ))
    )
)


(setf blueprints (cons nil (mapcar #'build-blueprint (mapcar #'extract-numbers (load-input 19)))))

(pdebug blueprints)

(defstruct state
    blueprint
    ore
    ore-bot
    clay
    clay-bot
    obsidian
    obsidian-bot
    geode
    geode-bot
    time-left
    factory
)

(defun start-state (blueprint)
    (make-state
        :blueprint blueprint
        :ore 0
        :ore-bot 1
        :clay 0
        :clay-bot 0
        :obsidian 0
        :obsidian-bot 0
        :geode 0
        :geode-bot 0
        :time-left 24
    )
)

(defun generate (state)
    (make-state
        :blueprint (state-blueprint state)
        :ore (+ (state-ore state) (state-ore-bot state))
        :ore-bot (+ (state-ore-bot state) (if (eql (state-factory state) :ore) 1 0))
        :clay (+ (state-clay state) (state-clay-bot state))
        :clay-bot (+ (state-clay-bot state) (if (eql (state-factory state) :clay) 1 0))
        :obsidian (+ (state-obsidian state) (state-obsidian-bot state))
        :obsidian-bot (+ (state-obsidian-bot state) (if (eql (state-factory state) :obsidian) 1 0))
        :geode (+ (state-geode state) (state-geode-bot state))
        :geode-bot (+ (state-geode-bot state) (if (eql (state-factory state) :geode) 1 0))
        :time-left (- (state-time-left state) 1)
        :factory nil
    )
)

(defun validate (state)
    (if (and
        (>= (state-ore state) 0)
        (>= (state-clay state) 0)
        (>= (state-obsidian state) 0)
        (>= (state-geode state) 0)
        (<= (state-ore state) 7)
        (<= (state-clay-bot state) 7)
        (<= (state-geode state) (extract-cost (elt blueprints (state-blueprint state)) :obsidian :geode))
    ) state nil)
)

(defun build-bot (state resource)
    (let(
        (bp (elt blueprints (state-blueprint state)))
    )
    (make-state
        :blueprint (state-blueprint state)
        :ore (- (state-ore state) (extract-cost bp :ore resource))
        :ore-bot (state-ore-bot state)
        :clay (- (state-clay state) (extract-cost bp :clay resource))
        :clay-bot (state-clay-bot state)
        :obsidian (- (state-obsidian state) (extract-cost bp :obsidian resource))
        :obsidian-bot (state-obsidian-bot state)
        :geode (- (state-geode state) (extract-cost bp :geode resource))
        :geode-bot (state-geode-bot state)
        :time-left (state-time-left state)
        :factory resource
    )
    )
)


(defun next-states (state)
    ;;(list (car (reverse (
    (reverse (cons (generate state) (remove nil 
        (loop for bots in (list :ore :clay :obsidian :geode) collect
            (validate (build-bot (generate state) bots))
        )
    )))
    ;;)))
)

(defun next-states-all (states loops)
    (if (<= loops 0) states (next-states-all (mapcan #'next-states states) (- loops 1)))
)

(defun heuristic (st)
    (let* (
        (geodes (state-geode st))
        (bots (state-geode-bot st))
        (time- (state-time-left st))
        (bot-efficient (* bots time-))
        (bot-ifbuild (floor (* time- (- time- 1)) 2))
    )
        (- (+
            (* 100000 (+ geodes bot-efficient))
            (* 1000 time-)
            (* 1 (state-obsidian-bot st))
        ))
    )
)


(defun prune-score (st)
    (+
        (* 8 (state-geode-bot st))
        (* 4 (state-obsidian-bot st))
        (* 2 (state-clay-bot st))
        (* 1 (state-ore-bot st))
    )
)

(defparameter *queue* (make-instance 'cl-heap:priority-queue))

(defparameter best-scores (loop for x from 0 to 30 collect 0))

(defun validate-state (state)
    (let* (
        (best-score (nth (state-time-left state) best-scores))
        (current-score (prune-score state))
        (treshhold-score (floor best-score 2))
    )
    (progn
        (when (> current-score best-score)
            (setf (nth (state-time-left state) best-scores) current-score)
        )
        (if (>= current-score treshhold-score)
            state 
            nil
        )
    )
    )
)

(defun store-state (state)
    (let (
        (vstate (validate-state state))
    )
    (when (not (null vstate))
        (cl-heap:enqueue *queue* state (heuristic state))
    )
    )
)

(defun strictly-worse (a b)
    (and
        (<= (state-ore a) (state-ore b))
        (<= (state-ore-bot a) (state-ore-bot b))
        (<= (state-clay a) (state-clay b))
        (<= (state-clay-bot a) (state-clay-bot b))
        (<= (state-obsidian a) (state-obsidian b))
        (<= (state-obsidian-bot a) (state-obsidian-bot b))
        (<= (state-geode a) (state-geode b))
        (<= (state-geode-bot a) (state-geode-bot b))
        (<= (state-time-left a) (state-time-left b))
        (null (state-factory a))
        (= (state-blueprint a) (state-blueprint b))
    )
)

(defun pickup-next-state ()
    (loop while t do
        (let (
            (top (validate-state (cl-heap:dequeue *queue*)))
        )
            (when (not (null top))
                (return-from pickup-next-state top)
            )
        )
    )
)


(defparameter past-states nil)
(defun search-optimal-state ()
    (setq best-remaining 24)
    
    (store-state (start-state 2))
    
    (loop while t do
        (let (
            (top (pickup-next-state))
        )
            (progn
                (setf past-states (cons top past-states))
                (when (< (state-time-left top) best-remaining)
                    (progn
                        (setq best-remaining (state-time-left top))
                        (format t "current: ~a ~a ~a ~a ~a" best-remaining top (heuristic top) (cl-heap:queue-size *queue*) best-scores)
                        (terpri)
                    )
                )
                (if (= (state-time-left top) 0)
                    (return-from search-optimal-state top)
                    (loop for nxt in (next-states top) do
                        (store-state nxt)
                    )
                )
            )
        )
    )
)

(pdebug (search-optimal-state))
