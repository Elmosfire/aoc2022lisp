(load "aoc.lisp")
(ql:quickload :cl-ppcre)
(ql:quickload :split-sequence)


(defun nest ()
    "When supplied with the strings of the input data, returns a nested list with the food supplies of each elf"
    (split-sequence:split-sequence "" (load-input 11) :test #'equal)
)

(defun fancy-parse-int (string)
    (if (string= string "*") 12 
    (if (string= string "+") 15 
        (parse-integer string)
    ))
)

(defun extract-numbers-and-operators (string)
    (mapcar #'fancy-parse-int (cl-ppcre:all-matches-as-strings (cl-ppcre:parse-string "[0-9*+]+") string))
)

(defstruct monkey-spec
    identifier
    starting-items
    operation-type
    operation-value
    div-test
    target-true
    target-false
)

(defun extract-operation-type (operation-line)
    (if (null (cdr operation-line)) :square (if (= (car operation-line) 12) :mult :sum))
)

(defun extract-operation-value (operation-line)
    (if (null (cdr operation-line)) 0 (cadr operation-line))
)

(defun build-monkey-spec (identifier input-block)
    (let (
        (idn (extract-numbers-and-operators identifier))
        (input-block-proc  (mapcar #'extract-numbers-and-operators input-block))
    )
    (make-monkey-spec
            :identifier (car idn)
            :starting-items (nth 0 input-block-proc)
            :operation-type (extract-operation-type (nth 1 input-block-proc))
            :operation-value (extract-operation-value (nth 1 input-block-proc))
            :div-test (car (nth 2 input-block-proc))
            :target-true (car (nth 3 input-block-proc))
            :target-false (car (nth 4 input-block-proc))
        )
    )
)


(defun build-all-monkeys ()
    (mapcar #'(lambda (x) (build-monkey-spec (car x)  (cdr x))) (nest))
)

(setq monkey-specs (build-all-monkeys))

(defun $monkey (identifier)
    (find-if (lambda (x) (= (monkey-spec-identifier x) identifier)) monkey-specs)
)

(defstruct monkey-item ()
    current-holder
    item-value
)

(defun monkey-inspect (value monkey)
    (let (
        (monkey-type (monkey-spec-operation-type monkey))
        (monkey-value (monkey-spec-operation-value monkey))
    )
    (ecase monkey-type
        (:sum (+ value monkey-value))
        (:mult (* value monkey-value))
        (:square (* value value))
    )
    )
)

(setq lcm
    (reduce #'*
        (mapcar #'monkey-spec-div-test monkey-specs)
    )
)

(defun process-item (item decr)
    (let* (
        (monkey ($monkey (monkey-item-current-holder item)))
        (value (monkey-item-item-value item))
        (new-worry (rem (floor (monkey-inspect value monkey) (if decr 3 1)) lcm))
        (div-test (monkey-spec-div-test monkey))
        (divisible (= (rem new-worry div-test) 0))
    )
    (make-monkey-item 
        :current-holder (if divisible (monkey-spec-target-true monkey) (monkey-spec-target-false monkey))
        :item-value new-worry
    )
    )
)

(setq queues (list nil nil nil nil nil nil nil nil))



(defun store-monkey-item (item)
    (let (
        (index (monkey-item-current-holder item))
    )
    (setf (nth index queues) (cons item (nth index queues)))
    )
)

(defun clear-monkey (index)
    (setf (nth index queues) nil)
)

(defun store-initial-items (index)
    (loop for item in (monkey-spec-starting-items ($monkey index))
        do (store-monkey-item
            (make-monkey-item 
                :current-holder index
                :item-value item
            )
        )
    )
)

(defun monkey-indices ()
    (mapcar #'monkey-spec-identifier monkey-specs)
)

(loop for monkey in (monkey-indices)
    do (store-initial-items monkey)
)

(defun current-monkey-status ()
    (mapcar #'(lambda (x) (mapcar #'monkey-item-item-value (reverse x))) queues)
)

(defun current-monkey-verify ()
    (mapcar #'(lambda (x) (mapcar #'monkey-item-current-holder (reverse x))) queues)
)

(setq inspect-counter (list 0 0 0 0 0 0 0 0))

(defun increase-counter (index)
    (setf (nth index inspect-counter) (+ (nth index inspect-counter) 1))
)

(defun process-and-store (item decr)
    (progn
        (increase-counter (monkey-item-current-holder item))
        (store-monkey-item (process-item item decr))
    )
)

(defun process-monkey (index decr)
    (progn
        (loop for item in (nth index queues)
            do (process-and-store item decr)
        )
        (clear-monkey index)
    )
)

(defun process-round (repeat decr)
    (loop for i from 1 to repeat do (progn
        (pdebug i)
        (mapcar #'(lambda (x) (process-monkey x decr)) (monkey-indices))
    ))
)

(defun sol0 ()
    (let (
        (best (sort inspect-counter #'>))
    )
    (* (car best) (cadr best))
    )
)

;; this piece does p1
;;(pdebug (current-monkey-status))
;;(pdebug (current-monkey-verify))
;;(process-round 20 t)
;;(pdebug (current-monkey-status))
;;(sol (sol0) nil)

;; this piece does p2
(pdebug (current-monkey-status))
(process-round 10000 nil)
(pdebug (current-monkey-status))
(sol nil (sol0))


