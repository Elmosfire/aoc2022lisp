(load "aoc.lisp")
(ql:quickload :split-sequence)

(defun nest (seq)
    "When supplied with the strings of the input data, returns a nested list with the food supplies of each elf"
    (split-sequence:split-sequence "" seq :test #'equal)
)

(defun calories (seq)
    "When supplied with the strings of the input data, returns a nested list with the total calories of each elf"
    (mapcar #'(lambda(x) (reduce #'+ (mapcar #'parse-integer x))) (nest seq))
)


(defun best-n-elves (seq count)
    (reduce #'+ (subseq (sort (calories seq) #'>) 0 count))
)

(format t "part1: ~a" (best-n-elves (load-input 1) 1))
(terpri)
(format t "part2: ~a" (best-n-elves (load-input 1) 3))





