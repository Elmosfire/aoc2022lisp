(load "aoc.lisp")


(defun split-sack (seq)
    (list (subseq seq 0 (/ (length seq) 2)) (subseq seq (/ (length seq) 2) (length seq)))
)




(defun value (char)
    (+ (if (> (char-code char) (char-code #\_))
        (- (char-code char) (char-code #\a))
        (+ (- (char-code char) (char-code #\A)) 26)
    ) 1)
)

(defun recursive-intersection (seq)
    (if (null (cdr seq)) (car seq) (intersection (car seq) (recursive-intersection (cdr seq))))
)

(defun single-match-prio (seq)
    (value (car (recursive-intersection (mapcar #'to-char-list seq))))
)

(defun groups3 (seq)
    (if (null seq) nil (cons (list (nth 0 seq) (nth 1 seq) (nth 2 seq))  (groups3 (subseq seq 3 nil) )))
)

(format t "part1: ~S" (reduce #'+ (mapcar #'single-match-prio (mapcar #'split-sack (load-input 3)))))
(terpri)
(format t "part2: ~S" (reduce #'+ (mapcar #'single-match-prio  (groups3 (load-input 3)))))
(terpri)