(load "aoc.lisp")
(ql:quickload :split-sequence)

(defun nest ()
    "When supplied with the strings of the input data, returns a nested list with the food supplies of each elf"
    (split-sequence:split-sequence "" (load-input 13) :test #'equal)
)

(defun convert-input-line (line)
    (let* (
        (tmp0 (cl-ppcre:regex-replace-all "\\[" line "("))
        (tmp1 (cl-ppcre:regex-replace-all "\\]" tmp0 ")"))
        (tmp2 (cl-ppcre:regex-replace-all "," tmp1 " "))
    )
    (read-from-string tmp2)
    )
)



(defun check-both-types (e1 e2 t1 t2)
    (and (typep e1 t1) (typep e2 t2))
)

(defun compare-integers (a b)
    (if (< a b)
        :valid
    (if (> a b)
        :invalid
        :unknown
    ))
)

(defun compare-lists (a b)
    (progn ;;( format t "list (fr): ~a <> ~a" a b)(terpri)
    ( let (
        (comp-first (validate (car a) (car b)))
    )
    (if (eql comp-first :valid)
        :valid
    (if (eql comp-first :invalid)
        :invalid
        (validate (cdr a) (cdr b))
    ))
    )
    )
)

(defun compare-lists-guard (a b)
    (progn ;;( format t "list (gd): ~a <> ~a" a b)(terpri)
    (if (null a)
        (if (null b)
            :unknown
            :valid
        )
        (if (null b)
            :invalid
            (compare-lists a b)
        )
    )
    )
)


(defun validate (a b)
    (progn ( format t "general  : ~a <> ~a" a b)(terpri)
    (if (check-both-types a b 'integer 'integer)
        (compare-integers a b)
    (if (check-both-types a b 'list 'integer)
        (compare-lists-guard a (list b))
    (if (check-both-types a b 'integer 'list)
        (compare-lists-guard (list a) b)
    (if (check-both-types a b 'list 'list)
        (compare-lists-guard a b)
    ))))
    )
)
    
(defun validate-from-pair (pair)
    (progn ;;(format t "----")(terpri)
    (apply #'validate pair)
    )
)

(defun count-index (lst)
    (reduce #'+ (mapcar #'(lambda (x) (if (eql (cadr x) :valid) (+ 1 (car x)) 0)) (enumerate lst)))
)

(defun sorting-func (packet1 packet2)
    (eql (validate packet1 packet2) :valid)
)

;;(pdebug (count-index (mapcar #'validate-from-pair (mapcaar #'convert-input-line (nest)))))

(defun p2input ()
    (mapcar #'convert-input-line (cons "[[6]]" (cons "[[2]]" (mapcan #'(lambda (x) x) (nest)))))
)

(defun magic-car (a)
    (if (typep a 'list) (if (null a) nil (if (null (cdr a)) (car a) nil)) nil)
)

(defun extract-values (a)
    (if (null (cadr a)) 1 (if (typep (cadr a) 'integer) (if (or (= (cadr a) 2) (= (cadr a) 6)) (car a) 1) 1))
)

(pdebug (reduce #'* (mapcar #'extract-values (enumerate (cons nil (mapcar #'magic-car (mapcar #'magic-car (sort (p2input) #'sorting-func))))))))